const express = require("express");
const app = express();
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const cors = require("cors");
const dbConfig = require("./server/app/config/db.config");
const api = require('./server/app/routes/index');
const swaggerJsdoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const initdb = require('./server/app/scripts/initdb');
const options = require('./server/app/config/swagger.config');
const db = require("./server/app/models");

const Role = db.role;
const User = db.user;
const Speciality = db.speciality;
const Reason = db.reason;
const Symptom = db.symptom;
const Desease = db.desease;
const Medicine = db.medicine;
const Clinic = db.clinic;
const Doctor = db.doctor;
const Slot = db.slot;

const specs = swaggerJsdoc(options);

app.use(
  "/api-docs",
  swaggerUi.serve,
  swaggerUi.setup(specs)
);


db.mongoose
  .connect(`mongodb://${dbConfig.HOST}:${dbConfig.PORT}/${dbConfig.DB}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Successfully connect to MongoDB.");
    initial();
  })
  .catch(err => {
    console.error("Connection error", err);
    process.exit();
});

const corsOptions = {
  origin: ["http://localhost:4200", 'https://smarthelp-f3c12.web.app']
};

app.use(cors(corsOptions));
// parse requests of content-type - application/json
app.use(express.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

app.use('/api', api);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

async function initial() {
  console.log('Идет заполнение базы данных...');
  const rolesCount = await Role.estimatedDocumentCount();
  if (rolesCount === 0) {
    await initdb.createRoles();
    console.log('Роли успешно созданы')
  }
  const usersCount = await User.estimatedDocumentCount();
  if (usersCount === 0) {
    await initdb.createUsers();
    console.log('Пользователи успешно созданы')
  }
  const specsCount = await Speciality.estimatedDocumentCount();
  if (specsCount === 0) {
    await initdb.createSpecialities();
    console.log('Специальности успешно созданы')
  }
  const reasonsCount = await Reason.estimatedDocumentCount();
  if (reasonsCount === 0) {
    await initdb.createReasons();
    console.log('Причины успешно созданы')
  }
  const symptomsCount = await Symptom.estimatedDocumentCount();
  if (symptomsCount === 0) {
    await initdb.createSymptoms();
    console.log('Симптомы успешно созданы')
  }
  const deseasesCount = await Desease.estimatedDocumentCount();
  if (deseasesCount === 0) {
    await initdb.createDeseases();
    console.log('Болезни успешно созданы')
  }
  const medicineCount = await Medicine.estimatedDocumentCount();
  if (medicineCount === 0) {
    await initdb.createMedicine();
    console.log('Препараты успешно созданы')
  }
  const clinicCount = await Clinic.estimatedDocumentCount();
  if (clinicCount === 0) {
    await initdb.createClinic();
    console.log('Клиники успешно созданы')
  }
  const doctorCount = await Doctor.estimatedDocumentCount();
  if (doctorCount === 0) {
    await initdb.createDoctors();
    console.log('Доктора успешно добавлены');
  }
  const slotsCount = await Slot.estimatedDocumentCount();
  if (slotsCount === 0) {
    await initdb.createDoctorClinicSlots();
    console.log('Слоты успешно добавлены');
  }
  console.log('База заполнена');
}

exports.app = app;