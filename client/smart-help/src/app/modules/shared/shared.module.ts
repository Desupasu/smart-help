import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { MenuComponent } from './menu/menu.component';
import { CardComponent } from './card/card.component';
import { MaterialModule } from 'src/app/material.module';



@NgModule({
  declarations: [
    HeaderComponent,
    MenuComponent,
    CardComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports:[
    HeaderComponent,
    MenuComponent,
    CardComponent
  ]
})
export class SharedModule { }
