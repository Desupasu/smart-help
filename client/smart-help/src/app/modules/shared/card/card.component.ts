import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit, AfterViewInit {

  constructor() { }

  @Input() view: number = 1;
  @ViewChild("card") card: ElementRef | undefined;

  views: Object = {
    1: "card-1",
    2: "card-2",
    3: "card-3"
  }

  ngOnInit(): void {
  }

  ngAfterViewInit (): void {
    // console.log(this.card);
    // console.log(typeof(this.card.nativeElement));

    let card: HTMLElement = this.card.nativeElement;
    let view = this.views[this.view];
    if(view){
      card.classList.add(view);
    }
  }

}
