import { Component, Input, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private titleService: Title) { }

  @Input() public title: string;

  name: string = "Иванов Иван Б."

  ngOnInit(): void {
  }

}
