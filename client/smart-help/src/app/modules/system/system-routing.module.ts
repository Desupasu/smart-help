import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardComponent } from '../shared/card/card.component';
import { OnlineRecordComponent } from './online-record/online-record.component';

const routes: Routes = [
  {
    path: "online-record",
    component: OnlineRecordComponent
  },
  {
    path: "card",
    component: CardComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class SystemRoutingModule { }
