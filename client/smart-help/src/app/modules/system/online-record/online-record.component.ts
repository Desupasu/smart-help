import { AfterViewInit, Component, ElementRef, HostListener, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { CardComponent } from '../../shared/card/card.component';

@Component({
  selector: 'app-online-record',
  templateUrl: './online-record.component.html',
  styleUrls: ['./online-record.component.scss']
})
export class OnlineRecordComponent implements OnInit, AfterViewInit {

  constructor(private titleService: Title ) { }

  @ViewChild("test") private testRef: ElementRef | undefined;
  @ViewChild("wrapper") private wrapperRef: ElementRef | undefined;
  @ViewChildren(CardComponent) private cards: QueryList<CardComponent> | undefined;
  @HostListener('window:resize', ['$event'])
  onResize() {
    this.test.style.marginLeft = `${this.wrapper.clientWidth/2 - 255}px`;
    // console.log(wrapper.clientWidth);
    
  }

  public title: string = "Онлайн-запись ко врачу";
  private move = 0;
  private currentCard = 0;
  private test: HTMLElement;
  private wrapper: HTMLElement;
  

  ngOnInit(): void {
    this.titleService.setTitle(this.title);
  }

  ngAfterViewInit (): void {
    this.test = this.testRef.nativeElement;
    this.wrapper = this.wrapperRef.nativeElement;
    this.onResize();
    console.log(this.cards);
    
  }

  shift(direction: number){
    let move = this.move;
    let currentCard = this.currentCard;
      // function getNumberOfPx(str: string): number{
      //   let str_num = Number(str.slice(0, str.length - 2));
      //   return str_num;
      // }
      let cardWidth = this.cards.first.card.nativeElement.clientWidth;
      let shift: number = 80 + cardWidth;
      switch(direction){
        case -1:
          move -= shift;
          ++currentCard;
          break;
        case 1: 
          move += shift;
          --currentCard;
          break;
      }
      // console.log(this.move, currentCard, shift, this.cards.first.card, card, card_styles.marginRight, card.clientWidth, card_styles.borderRightWidth);
      if(currentCard >= 0 && currentCard < this.cards?.length){
        this.move = move;
        this.currentCard = currentCard;
        this.test.style.transform = `translateX(${this.move}px)`;
      }
  }

}
