import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OnlineRecordComponent } from './online-record.component';

describe('OnlineRecordComponent', () => {
  let component: OnlineRecordComponent;
  let fixture: ComponentFixture<OnlineRecordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OnlineRecordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OnlineRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
