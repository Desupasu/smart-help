import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SystemRoutingModule } from './system-routing.module';
import { OnlineRecordComponent } from './online-record/online-record.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    OnlineRecordComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    SystemRoutingModule
  ]
})
export class SystemModule { }
