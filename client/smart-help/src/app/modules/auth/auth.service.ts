import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  register(user): Observable<any>{
    return this.http.post(`${environment.apiUrl}/auth/signup`, user);
  }

  login(user): Observable<any>{
    return this.http.post(`${environment.apiUrl}/auth/signin`, user);
  }
}
