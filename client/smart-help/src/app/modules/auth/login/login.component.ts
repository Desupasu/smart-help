import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavigationService } from 'src/app/services/navigation.service';
import { environment } from 'src/environments/environment.prod';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private nav: NavigationService,
    private authService: AuthService
  ) { }

  public form_login: FormGroup;

  ngOnInit(): void {
    this.form_login = this.formBuilder.group({
      login: ["", [Validators.required]],
      password: ["", [Validators.required]]
    });
  }

  onLogin(){
    let user = {
      email: "",
      policy: "",
      password: this.password.value
    };
    if(this.form_login.valid){
      if(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]{2,6}$/.test(this.form_login.value.login)){
        user.email = this.login.value; 
      } else {
        user.policy = this.login.value;
      }
      this.authService.login(user).subscribe(data => {
        console.log(data);
        if(data?.payload){
          localStorage.setItem(environment.userLC, JSON.stringify(data.payload));
        }
      })
    }
  }

  forgotPassword(){}

  toRegistration(){
    this.nav.toPage('/registration');
  }

  public get login() {
    return this.form_login.get('login');
  }

  public get password() {
    return this.form_login.get('password');
  }
}