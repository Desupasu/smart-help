import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavigationService } from 'src/app/services/navigation.service';
import { environment } from 'src/environments/environment.prod';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private nav: NavigationService,
    private authService: AuthService
  ) { }

  public form_register: FormGroup;

  ngOnInit(): void {
    this.form_register = this.formBuilder.group({
      name: ["", [Validators.required]],
      surname: ["", [Validators.required]],
      patronymic: ["", [Validators.required]],
      email: ["", [Validators.required,  Validators.pattern(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]{2,6}$/)]],
      policy: ["", [Validators.required]],
      password: ["", [Validators.required]],
      repeatPassword: ["", [Validators.required]]
    });
  }

  onRegister(){
    if(this.form_register.valid){
      console.log(this.form_register.value);
      this.authService.register(this.form_register.value).subscribe(data => {
        console.log(data);
        if(data?.payload){
          localStorage.setItem(environment.userLC, JSON.stringify(data.payload));
        }
      });
    }
  }

  public get name() {
    return this.form_register.get('name');
  }

  public get surname() {
    return this.form_register.get('surname');
  }

  public get patronymic() {
    return this.form_register.get('patronymic');
  }

  public get email() {
    return this.form_register.get('email');
  }

  public get policy() {
    return this.form_register.get('policy');
  }

  public get password() {
    return this.form_register.get('password');
  }

  public get repeatPassword() {
    return this.form_register.get('repeatPassword');
  }

}
