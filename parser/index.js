const puppeteer = require('puppeteer');
const fs = require('file-system');
var faker = require('faker/locale/ru');

const getAllDeseases = async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('https://medihost.ru/zabolevanya', { waitUntil: 'networkidle2' });
  const result = await page.evaluate(() => {
      return Array.from(document.querySelectorAll('li.ilness a')).map(item => ({
          name: item.textContent,
          href: item.getAttribute('href')
      }));
  });

  fs.writeFileSync('deseases2.json', JSON.stringify(result));
  await browser.close();
};

const getAll = async () => {
    const browser = await puppeteer.launch({ headless: false });
    const page = await browser.newPage();
    await page.goto('https://2doc.by/wiki/diseases', { waitUntil: 'networkidle2' });
    const handles = await page.$$('ul + button');
    for (const handle of handles) {
        await handle.click();
    }
    const result = await page.evaluate(() => {
        return Array.from(document.querySelectorAll('li > a.text-link')).map(item => ({
            name: item.textContent,
            href: item.getAttribute('href')
        })).slice(2);
    });
    let symptoms = [];
    let reasons = [];
    let doctors = [];
    let finalResult = [];
  
   for (item of result) {
        const newPage = await browser.newPage();
        await newPage.goto(`https://2doc.by${item.href}`, { waitUntil: 'networkidle2' });
        const symptomResult = await newPage.evaluate(() => {
            return Array.from(document.querySelectorAll('#wiki-symptoms ul li')).map(n => n.textContent.replace(/\s?\(.+?\)/g, '').trim());
        });
        const reasonResult = await newPage.evaluate(() => {
            return Array.from(document.querySelectorAll('#wiki-reasons ul li')).map(n => n.textContent.replace(/\s?\(.+?\)/g, '').trim());
        });
        const doctorResult = await newPage.evaluate(() => {
            return Array.from(document.querySelectorAll('#wiki-doctors > h3 + ul > li')).map(n => n.textContent.replace(/\s?\(.+?\)/g, '').trim());
        });
        const mainDoctorResult = await newPage.evaluate(() => {
            return document.querySelector('#wiki-doctors > h3:first-of-type').textContent.trim();
        });
        const aboutResult = await newPage.evaluate(() => {
            return document.querySelector('#wiki-about .wysiwyg > p:first-child').textContent.trim();
        });

        if (symptomResult) {
            symptoms = symptoms.concat(symptomResult);
        }
        if (reasonResult) {
            reasons = reasons.concat(reasonResult);
        }
        if (doctorResult) {
            doctors = doctors.concat(doctorResult);
        }
        finalResult.push({
            name: item.name,
            description: aboutResult,
            mainDoctor: mainDoctorResult,
            reasons: reasonResult,
            symptoms: symptomResult,
            doctors: doctorResult,
        });
        newPage.close();
    }

    fs.writeFileSync('symptoms.json', JSON.stringify([...new Set(symptoms)]));
    fs.writeFileSync('reasons.json', JSON.stringify([...new Set(reasons)]));
    //fs.writeFileSync('doctors.json', JSON.stringify([...new Set(doctors)]));
    fs.writeFileSync('deseases.json', JSON.stringify(finalResult));
    await browser.close();
};

const getAllDoctors = async () => {
    const browser = await puppeteer.launch({ headless: false });
    const page = await browser.newPage();
    await page.goto('https://docdoc.ru/doctor', { waitUntil: 'networkidle2' });
    //await page.waitForSelector();
    await page.click('.search-form__input--specialities input');
    const result = await page.evaluate(() => {
        return Array.from(document.querySelectorAll('.search-form__input--specialities .v-autocomplete-list > *')).map(item => item.textContent.trim());
    });
    fs.writeFileSync('doctors.json', JSON.stringify(result.slice(1)));
    await browser.close();
};


const getAllRealDoctors = async () => {
    const browser = await puppeteer.launch({ headless: false });
    const page = await browser.newPage();
    await page.goto('https://nn.docdoc.ru/doctor', { waitUntil: 'networkidle2' });

    let realDoctors = [];
    let clinic = [];
    for(let i = 2; i <=27; i++) {
        // await page.waitForSelector('slots-buttons__expander');
        // const handles = await page.$$('.slots-buttons__expander');
        // for (const handle of handles) {
        //     await handle.click();
        // }
        const result = await page.evaluate(() => {
            const currentDate = new Date().getFullYear();
            return Array.from(document.querySelectorAll('.doctor-list-page-card__right')).map(item => {
                const fio = item.querySelector('.doctor-list-page-card__details a').textContent.trim().split(' ');
                return {
                    specialities: item.querySelector('.doctor-list-page-card__details > div:first-child').textContent.split('·').map(n => n.replace(/\s?\(.+?\)/g, '').trim()),
                    name: fio[1],
                    surname: fio[0],
                    lastName: fio[2],
                    startWork: `${currentDate - item.querySelector('.doctor-list-page-card__details > div:last-child > span:first-child').textContent.replace(/\D/g, '').trim()}-01-01T00:00:00`,
                    clinics: [item.querySelector('.doctor-card-clinic__link').textContent.trim()],
                    address: item.querySelector('.doctor-card-clinic__address').textContent.trim(),
                }
            });
        });
        realDoctors = realDoctors.concat(result.map(item => ({
            ...item,
            email: faker.internet.email(),
            password: faker.internet.password(),
            address: undefined,
        })));
        result.forEach(item => {
            if (clinic.findIndex(m => m.name === item.name) === -1) {
                clinic.push({
                    name: item.clinics[0],
                    address: item.address,
                    workTime: [{
                        name: 'Понедельник',
                        st_date: '8:00',
                        end_date: '19:00'
                    },
                    {
                        name: 'Вторник',
                        st_date: '8:00',
                        end_date: '19:00'
                    },
                    {
                        name: 'Среда',
                        st_date: '8:00',
                        end_date: '19:00'
                    },
                    {
                        name: 'Четверг',
                        st_date: '8:00',
                        end_date: '19:00'
                    },
                    {
                        name: 'Пятнинца',
                        st_date: '8:00',
                        end_date: '19:00'
                    },
                    {
                        name: 'Суббота',
                        st_date: '9:00',
                        end_date: '18:00'
                    },
                    {
                        name: 'Воскресенье',
                        st_date: '9:00',
                        end_date: '18:00'
                    }]
                })
            }
        })
        await page.goto(`https://nn.docdoc.ru/doctor/page/${i}`, { waitUntil: 'networkidle2' });
    }
    await page.goto('https://docdoc.ru/doctor', { waitUntil: 'networkidle2' });
    for(let i = 2; i <=27; i++) {
        const result = await page.evaluate(() => {
            document.querySelector('.slots-buttons__expander').click();
            return Array.from(document.querySelectorAll('.doctor-list-page-card__right')).map(item => {
                return Array.from(item.querySelectorAll('.slots-buttons__item')).map((item, index, arr) => {
                        if (index === arr.length - 1) return undefined;
                        return [{
                            st_date: `2022-04-12T${item.textContent.trim()}:00`,
                            end_date: `2022-04-12T${arr[index + 1].textContent.trim()}:00`
                        },
                        {
                            st_date: `2022-04-13T${item.textContent.trim()}:00`,
                            end_date: `2022-04-13T${arr[index + 1].textContent.trim()}:00`
                        },
                        {
                            st_date: `2022-04-14T${item.textContent.trim()}:00`,
                            end_date: `2022-04-14T${arr[index + 1].textContent.trim()}:00`
                        },
                        {
                            st_date: `2022-04-15T${item.textContent.trim()}:00`,
                            end_date: `2022-04-15T${arr[index + 1].textContent.trim()}:00`
                        },
                        {
                            st_date: `2022-04-16T${item.textContent.trim()}:00`,
                            end_date: `2022-04-16T${arr[index + 1].textContent.trim()}:00`
                        },
                        {
                            st_date: `2022-04-17T${item.textContent.trim()}:00`,
                            end_date: `2022-04-17T${arr[index + 1].textContent.trim()}:00`
                        }]
                    }).filter(Boolean).flat()
            });
        });
        result.forEach((item, index) => {
            realDoctors[index * (i - 2)].meets = item;
        })
        await page.goto(`https://docdoc.ru/doctor/page/${i}`, { waitUntil: 'networkidle2' });
    }
    fs.writeFileSync('realdoctors2.json', JSON.stringify(realDoctors));
    fs.writeFileSync('clinic.json', JSON.stringify(clinic));
    await browser.close();
};
 
getAllRealDoctors();
