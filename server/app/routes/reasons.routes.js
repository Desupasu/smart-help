const { authJwt } = require("../middlewares");
const controller = require("../controllers/reasons.controller");
const router = require('express').Router();

router.use(function(req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});

router.get("/all", controller.getReasons);

router.get("/:id", controller.getReasonById);

router.post("/add", [authJwt.verifyToken, authJwt.hasSomeRole("admin", "moderator")], controller.postReasons);

router.put("/:id", [authJwt.verifyToken, authJwt.hasSomeRole("admin", "moderator")], controller.putReasons);

router.delete("/:id", [authJwt.verifyToken, authJwt.hasSomeRole("admin", "moderator")], controller.deleteReasons);

module.exports = router;

/**
 * @swagger
 * /reasons/all:
 *   get:
 *     summary: Получить причины болезней
 *     description: Получить причины болезней
 *     tags: [REASONS]
 *     parameters:
 *       - in: query
 *         name: search
 *         schema:
 *           type: string
 *           required: false
 *           description: Поиск по причине болезни
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: array
 *                  items:
 *                    allOf:
 *                      - $ref: '#/components/schemas/Reason'
 *                      - properties:
 *                          id:
 *                            type: string
 *                      
 *                       
 */

/**
 * @swagger
 * /reasons/all/{id}:
 *   get:
 *     summary: Получить причину по id
 *     description: Получить причину по id
 *     tags: [REASONS]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           required: true
 *           description: Id сущности
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: object
 *                  properties:
 *                    reason:
 *                      $ref: '#/components/schemas/Reason'
 *             
 */

/**
 * @swagger
 * /reasons/add:
 *   post:
 *     summary: Добавить причины болезней
 *     description: Добавить причины болезней
 *     tags: [REASONS]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               reasons:
 *                  type: array
 *                  items:
 *                     $ref: '#/components/schemas/Reason'
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: array
 *                  items:
 *                     allOf:
 *                     - $ref: '#/components/schemas/Reason'
 *                     - properties:
 *                         id:
 *                           type: string
 *                      
 *                       
 */

/**
 * @swagger
 * /reasons/{id}:
 *   put:
 *     summary: Изменить причину болезней
 *     description: Изменить причину болезней
 *     tags: [REASONS]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           required: true
 *           description: Id причины
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               reason:
 *                 $ref: '#/components/schemas/Reason'
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: object
 *                  properties:
 *                    reason:
 *                      allOf:
 *                        - $ref: '#/components/schemas/Reason'
 *                        - properties:
 *                            id:
 *                              type: string
 *                      
 *                       
 */

/**
 * @swagger
 * /reasons/{id}:
 *   delete:
 *     summary: Удалить причину болезней
 *     description: Удалить причину болезней
 *     tags: [REASONS]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           required: true
 *           description: Id причины
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: boolean
 *                  example: true
 *                      
 *                       
 */
