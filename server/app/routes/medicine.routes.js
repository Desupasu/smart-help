const { authJwt } = require("../middlewares");
const controller = require("../controllers/medicine.controller");
const router = require('express').Router();

router.use(function(req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});

router.get("/all", controller.getMedicines);

router.get("/:id", controller.getMedicineById);

router.post("/add", [authJwt.verifyToken, authJwt.hasSomeRole("admin", "moderator")], controller.postMedicines);

router.put("/:id", [authJwt.verifyToken, authJwt.hasSomeRole("admin", "moderator")], controller.putMedicines);

router.delete("/:id", [authJwt.verifyToken, authJwt.hasSomeRole("admin", "moderator")], controller.deleteMedicines);

module.exports = router;

/**
 * @swagger
 * /medicines/all:
 *   get:
 *     summary: Получить препараты
 *     description: Получить препараты
 *     tags: [MEDICINE]
 *     parameters:
 *       - in: query
 *         name: search
 *         schema:
 *           type: string
 *           required: false
 *           description: Поиск по названию препарата
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: array
 *                  items:
 *                    allOf:
 *                      - $ref: '#/components/schemas/Medicine'
 *                      - properties:
 *                          id:
 *                            type: string
 *                      
 *                       
 */

/**
 * @swagger
 * /medicines/all/{id}:
 *   get:
 *     summary: Получить препарат по id
 *     description: Получить препарат по id
 *     tags: [MEDICINE]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           required: true
 *           description: Id сущности
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: object
 *                  properties:
 *                    medicine:
 *                      $ref: '#/components/schemas/Medicine'
 *             
 */

/**
 * @swagger
 * /medicines/add:
 *   post:
 *     summary: Добавить препараты
 *     description: Добавить препараты
 *     tags: [MEDICINE]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               medicines:
 *                  type: array
 *                  items:
 *                     $ref: '#/components/schemas/Medicine'
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: array
 *                  items:
 *                     allOf:
 *                     - $ref: '#/components/schemas/Medicine'
 *                     - properties:
 *                         id:
 *                           type: string
 *                      
 *                       
 */

/**
 * @swagger
 * /medicines/{id}:
 *   put:
 *     summary: Изменить препараты
 *     description: Изменить препараты
 *     tags: [MEDICINE]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           required: true
 *           description: Id причины
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               medicine:
 *                 $ref: '#/components/schemas/Medicine'
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: object
 *                  properties:
 *                    medicine:
 *                      allOf:
 *                        - $ref: '#/components/schemas/Medicine'
 *                        - properties:
 *                            id:
 *                              type: string
 *                      
 *                       
 */

/**
 * @swagger
 * /medicines/{id}:
 *   delete:
 *     summary: Удалить препараты
 *     description: Удалить препараты
 *     tags: [MEDICINE]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           required: true
 *           description: Id причины
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: boolean
 *                  example: true
 *                      
 *                       
 */
