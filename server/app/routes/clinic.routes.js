const { authJwt } = require("../middlewares");
const controller = require("../controllers/clinic.controller");
const router = require('express').Router();

router.use(function(req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});

router.get("/all", controller.getClinics);

router.get("/all/:id", controller.getClinicById);

router.post("/add", [authJwt.verifyToken, authJwt.hasSomeRole("admin", "moderator")], controller.postClinics);

router.put("/:id", [authJwt.verifyToken, authJwt.hasSomeRole("admin", "moderator")], controller.putClinics);

router.delete("/:id", [authJwt.verifyToken, authJwt.hasSomeRole("admin", "moderator")], controller.deleteClinics);

module.exports = router;

/**
 * @swagger
 * /clinics/all:
 *   get:
 *     summary: Получить клиники
 *     description: Получить клиники
 *     tags: [CLINIC]
 *     parameters:
 *       - in: query
 *         name: search
 *         schema:
 *           type: string
 *           required: false
 *           description: Поиск по названию клиники
 *       - in: query
 *         name: speciality
 *         schema:
 *           type: string
 *           required: false
 *           description: Выбор клиник по специальности
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: array
 *                  items:
 *                    allOf:
 *                      - $ref: '#/components/schemas/Clinic'
 *                      - properties:
 *                          id:
 *                            type: string
 *             
 */

/**
 * @swagger
 * /clinics/all/{id}:
 *   get:
 *     summary: Получить клинику по id
 *     description: Получить клинику по id
 *     tags: [CLINIC]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           required: true
 *           description: Id сущности
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: object
 *                  properties:
 *                    clinic:
 *                      $ref: '#/components/schemas/Clinic'
 *             
 */


/**
 * @swagger
 * /clinics/add:
 *   post:
 *     summary: Добавить клиники
 *     description: Добавить клиники
 *     tags: [CLINIC]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               clinics:
 *                  type: array
 *                  items:
 *                     $ref: '#/components/schemas/Clinic'
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: array
 *                  items:
 *                     allOf:
 *                     - $ref: '#/components/schemas/Clinic'
 *                     - properties:
 *                         id:
 *                           type: string
 *                      
 *                       
 */

/**
 * @swagger
 * /clinics/{id}:
 *   put:
 *     summary: Изменить клиники
 *     description: Изменить клиники
 *     tags: [CLINIC]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           required: true
 *           description: Id причины
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               clinic:
 *                 $ref: '#/components/schemas/Clinic'
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: object
 *                  properties:
 *                    clinic:
 *                      allOf:
 *                        - $ref: '#/components/schemas/Clinic'
 *                        - properties:
 *                            id:
 *                              type: string
 *                      
 *                       
 */

/**
 * @swagger
 * /clinics/{id}:
 *   delete:
 *     summary: Удалить клиники
 *     description: Удалить клиники
 *     tags: [CLINIC]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           required: true
 *           description: Id причины
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: boolean
 *                  example: true
 *                      
 *                       
 */
