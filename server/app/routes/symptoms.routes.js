const { authJwt } = require("../middlewares");
const controller = require("../controllers/symptoms.controller");
const router = require('express').Router();

router.use(function(req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});

router.get("/all", controller.getSymptoms);

router.get("/:id", controller.getSymptomById);

router.post("/add", [authJwt.verifyToken, authJwt.hasSomeRole("admin", "moderator")], controller.postSymptoms);

router.put("/:id", [authJwt.verifyToken, authJwt.hasSomeRole("admin", "moderator")], controller.putSymptoms);

router.delete("/:id", [authJwt.verifyToken, authJwt.hasSomeRole("admin", "moderator")], controller.deleteSymptoms);

module.exports = router;

/**
 * @swagger
 * /symptoms/all:
 *   get:
 *     summary: Получить симптомы болезней
 *     description: Получить симптомы болезней
 *     tags: [SYMPTOMS]
 *     parameters:
 *       - in: query
 *         name: search
 *         schema:
 *           type: string
 *           required: false
 *           description: Поиск по симптому болезни
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: array
 *                  items:
 *                     allOf:
 *                        - $ref: '#/components/schemas/Symptom'
 *                        - properties:
 *                            id:
 *                              type: string
 *                      
 *                       
 */

/**
 * @swagger
 * /symptoms/all/{id}:
 *   get:
 *     summary: Получить симптом по id
 *     description: Получить симптом по id
 *     tags: [SYMPTOMS]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           required: true
 *           description: Id сущности
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: object
 *                  properties:
 *                    symptom:
 *                      $ref: '#/components/schemas/Symptom'
 *             
 */

/**
 * @swagger
 * /symptoms/add:
 *   post:
 *     summary: Добавить симптомы болезней
 *     description: Добавить симптомы болезней
 *     tags: [SYMPTOMS]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               symptoms:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Symptom'
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: array
 *                  items:
 *                      allOf:
 *                        - $ref: '#/components/schemas/Symptom'
 *                        - properties:
 *                            id:
 *                              type: string
 *                      
 *                       
 */

/**
 * @swagger
 * /symptoms/{id}:
 *   put:
 *     summary: Изменить симптом болезней
 *     description: Изменить симптом болезней
 *     tags: [SYMPTOMS]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           required: true
 *           description: Id симптома
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               reason:
 *                 $ref: '#/components/schemas/Symptom'
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: object
 *                  properties:
 *                    reason:
 *                      allOf:
 *                        - $ref: '#/components/schemas/Symptom'
 *                        - properties:
 *                            id:
 *                              type: string
 *         
 */

/**
 * @swagger
 * /symptoms/{id}:
 *   delete:
 *     summary: Удалить симптом болезней
 *     description: Удалить симптом болезней
 *     tags: [SYMPTOMS]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           required: true
 *           description: Id симптома
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: boolean
 *                  example: true             
 *                       
 */
