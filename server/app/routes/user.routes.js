const controller = require("../controllers/user.controller");
const { authJwt } = require("../middlewares");
const router = require("express").Router();

router.use(function (req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});

router.get("/me", [authJwt.isUser], controller.getMe);

router.put("/me", [authJwt.isUser], controller.changeMe);

router.post("/applyMeet", [authJwt.isUser], controller.applyMeet);

router.get("/getMeets", [authJwt.isUser], controller.getMeets);

module.exports = router;

/**
 * @swagger
 * components:
 *   schemas:
 *     UserResponse:
 *       type: object
 *       properties:
 *         message:
 *          type: string
 *         hasError:
 *          type: boolean
 *          description: Есть ли ошибка
 *          example: false
 *         payload:
 *          allOf:
 *            - $ref: '#/components/schemas/User'
 *      
 */

/**
 * @swagger
 * /user/me:
 *   put:
 *     summary: Изменить информацию о пользователе.
 *     description: Изменить информацию о пользователе.
 *     tags: [USER]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *                allOf:
 *                - $ref: '#/components/schemas/User'
 *                - properties:
 *                    usedMedicine:
 *                      type: array
 *                      items:
 *                        type: string
 *                    chronicDeseases:
 *                      type: array
 *                      items:
 *                        type: string
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/UserResponse'
 *                       
 */

/**
 * @swagger
 * /user/me:
 *   get:
 *     summary: Получить информацию о пользователе.
 *     description:  Получить информацию о пользователе.
 *     tags: [USER]
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *             $ref: '#/components/schemas/UserResponse'
 *                       
 */

/**
 * @swagger
 * /user/applyMeet:
 *   post:
 *     summary: Записаться на прием
 *     description: Записаться на прием
 *     tags: [USER]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *              type: object
 *              properties:
 *                  meet:
 *                    type: object
 *                    properties:
 *                      clinic:
 *                        type: string
 *                        required: true
 *                      doctor:
 *                        type: string
 *                        required: true
 *                      speciality:
 *                        type: string
 *                        required: true
 *                      symptoms:
 *                        type: array
 *                        items:
 *                          type: string
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: object
 *                  properties:
 *                   symptoms:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Symptom'
 *                   speciality:
 *                      $ref: '#/components/schemas/Speciality'
 *                   doctor:
 *                      $ref: '#/components/schemas/Doctor'
 *                   clinic:
 *                      $ref: '#/components/schemas/Clinic'
 *                   isOnline:
 *                      type: boolean
 *                      description: Запись онлайн
 *                   endDate:
 *                      type: string
 *                      description: ДатаВремя начала приема
 *                      example: 2022-04-14T14:15:00
 *                   stDate:
 *                      type: string
 *                      description: ДатаВремя конца приема
 *                      example: 2022-04-14T14:15:00
 *                       
 */

/**
 * @swagger
 * /user/getMeets:
 *   post:
 *     summary: Получить записи на прием пользователя
 *     description: Получить записи на прием пользователя
 *     tags: [USER]
 *     responses:
 *      200:
 *        content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               message:
 *                 type: string
 *               hasError:
 *                 type: boolean
 *                 description: Есть ли ошибка
 *                 example: false
 *               payload:
 *                 type: array
 *                 items:
 *                  type: object
 *                  properties:
 *                   symptoms:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Symptom'
 *                   speciality:
 *                      $ref: '#/components/schemas/Speciality'
 *                   doctor:
 *                      $ref: '#/components/schemas/Doctor'
 *                   clinic:
 *                      $ref: '#/components/schemas/Clinic'
 *                   isOnline:
 *                      type: boolean
 *                      description: Запись онлайн
 *                   endDate:
 *                      type: string
 *                      description: ДатаВремя начала приема
 *                      example: 2022-04-14T14:15:00
 *                   stDate:
 *                      type: string
 *                      description: ДатаВремя конца приема
 *                      example: 2022-04-14T14:15:00
 *                       
 */
