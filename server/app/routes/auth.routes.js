const { verifySignUp } = require("../middlewares");
const controller = require("../controllers/auth.controller");
const router = require('express').Router();

router.use(function(req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});

router.post(
  "/signup",
  [
    verifySignUp.checkPasswords,
    verifySignUp.checkDuplicatePolicyOrEmail,
    verifySignUp.checkRolesExisted,
  ],
  controller.signup
);

router.post("/signin", controller.signin);

module.exports = router;

/**
 * @swagger
 * components:
 *   schemas:
 *     LoginResponse:
 *       type: object
 *       properties:
 *         message:
 *          type: string
 *         hasError:
 *          type: boolean
 *          description: Есть ли ошибка
 *          example: false
 *         payload:
 *          allOf:
 *            - $ref: '#/components/schemas/User'
 *            - properties:
 *                accessToken:
 *                  type: string
 *      
 */

/**
 * @swagger
 * /auth/signup:
 *   post:
 *     summary: Зарегистрировать пользователя.
 *     description: Зарегистрировать пользователя.
 *     tags: [AUTH]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *            allOf:
 *             - $ref: '#/components/schemas/User'
 *             - properties:
 *                 usedMedicine:
 *                   type: array
 *                   items:
 *                     type: string
 *                 chronicDeseases:
 *                   type: array
 *                   items:
 *                     type: string
 *                 password:
 *                   type: string
 *                   example: QkgsdfU341
 *                 repeatPassword:
 *                   type: string
 *                   example: QkgsdfU341
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/LoginResponse'
 *                       
 */

/**
 * @swagger
 * /auth/signin:
 *   post:
 *     summary: Авторизовать пользователя.
 *     description: Авторизовать пользователя.
 *     tags: [AUTH]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *              required:
 *              - email
 *              - policy
 *              - password
 *              properties:
 *                 email:
 *                   type: string
 *                   example: ivan@mail.ru
 *                 policy:
 *                   type: string
 *                   example: 34672357
 *                 password:
 *                   type: string
 *                   example: QkgsdfU341
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/LoginResponse'
 *                       
 */