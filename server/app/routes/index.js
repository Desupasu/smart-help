const router = require("express").Router();
const { authJwt } = require("../middlewares");
const auth = require("./auth.routes");
const reasons = require("./reasons.routes");
const symptoms = require("./symptoms.routes");
const speciality = require("./speciality.routes");
const deseases = require("./deseases.routes");
const medicine = require("./medicine.routes");
const clinic = require("./clinic.routes");
const user = require("./user.routes");
const doctor = require("./doctor.routes");
const slots = require("./slots.routes");

router.use(function (req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});

router.use("/reasons", reasons);

router.use("/symptoms", symptoms);

router.use("/speciality", speciality);

router.use("/medicines", medicine);

router.use("/deseases", deseases);

router.use("/clinics", clinic);

router.use("/doctors", doctor);

router.use("/slots", slots);

router.use("/auth", auth);

router.use("/user", [authJwt.verifyToken], user);

module.exports = router;
