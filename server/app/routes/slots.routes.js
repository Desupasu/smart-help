const controller = require("../controllers/slot.controller");
const router = require('express').Router();

router.use(function(req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});

router.get("/allClinic", controller.getAllClinicSlots);
router.get("/allOnline", controller.getAllOnlineSlots);

module.exports = router;

/**
 * @swagger
 * /slots/allClinic:
 *   get:
 *     summary: Получить доступное время для записи в клинике
 *     description: Получить доступное время для записи в клинике
 *     tags: [SLOTS]
 *     parameters:
 *       - in: query
 *         name: clinic
 *         schema:
 *           type: string
 *           required: false
 *           description: id клиники
 *       - in: query
 *         name: doctor
 *         schema:
 *           type: string
 *           required: false
 *           description: id врача
 *       - in: query
 *         name: st_date
 *         schema:
 *           type: string
 *           required: true
 *           description: начала периода поиска
 *       - in: query
 *         name: end_date
 *         schema:
 *           type: string
 *           required: true
 *           description: конец периода поиска
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: array
 *                  items:
 *                    type: object
 *                    properties:
 *                      doctor:
 *                          $ref: '#/components/schemas/Doctor'     
 *                      items:
 *                          type: array
 *                          items:
 *                              type: object
 *                              properties:
 *                                  clinic:
 *                                     $ref: '#/components/schemas/Clinic'
 *                                  slots:
 *                                     type: array
 *                                     items:
 *                                        $ref: '#/components/schemas/Slot'     
 *                       
 */

/**
 * @swagger
 * /slots/allOnline:
 *   get:
 *     summary: Получить доступное время для записи онлайн
 *     description: Получить доступное время для записи онлайн
 *     tags: [SLOTS]
 *     parameters:
 *       - in: query
 *         name: clinic
 *         schema:
 *           type: string
 *           required: false
 *           description: id клиники
 *       - in: query
 *         name: doctor
 *         schema:
 *           type: string
 *           required: false
 *           description: id врача
 *       - in: query
 *         name: st_date
 *         schema:
 *           type: string
 *           required: true
 *           description: начала периода поиска
 *       - in: query
 *         name: end_date
 *         schema:
 *           type: string
 *           required: true
 *           description: конец периода поиска
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: array
 *                  items:
 *                    type: object
 *                    properties:
 *                      doctor:
 *                          $ref: '#/components/schemas/Doctor'     
 *                      items:
 *                          type: array
 *                          items:
 *                              type: object
 *                              properties:
 *                                  clinic:
 *                                     $ref: '#/components/schemas/Clinic'
 *                                  slots:
 *                                     type: array
 *                                     items:
 *                                        $ref: '#/components/schemas/Slot'     
 *                       
 */