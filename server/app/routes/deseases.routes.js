const { authJwt } = require("../middlewares");
const controller = require("../controllers/deseases.controller");
const router = require('express').Router();

router.use(function(req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});

router.get("/all", controller.getDeseases);

router.get("/all/:id", controller.getDeseaseById);

router.get("/bysymptom", controller.getDeseasesBySymptomsAndReasons);

router.post("/add", [authJwt.verifyToken, authJwt.hasSomeRole("admin", "moderator")], controller.postDeseases);

router.put("/all/:id", [authJwt.verifyToken, authJwt.hasSomeRole("admin", "moderator")], controller.putDeseases);

router.delete("/all/:id", [authJwt.verifyToken, authJwt.hasSomeRole("admin", "moderator")], controller.deleteDeseases);

module.exports = router;

/**
 * @swagger
 * /deseases/bysymptom:
 *   get:
 *     summary: Получить диагноз по симптомам и причинам
 *     description: Получить диагноз по симптомам и причинам
 *     tags: [DESEASES]
 *     parameters:
 *       - in: query
 *         name: reasons
 *         schema:
 *           type: array
 *           items:
 *            type: string
 *            required: false
 *       - in: query
 *         name: symptoms
 *         schema:
 *           type: array
 *           items:
 *            type: string
 *            required: false
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                  example: Необходимо добавить больше симптомов
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: object
 *                  properties:
 *                     isAddSymptom:
 *                        type: boolean
 *                        description: Нужно ли добавить еще симптомов
 *                        example: false
 *                     isAddReasonOrSymptom:
 *                        type: boolean
 *                        description: Нужно ли добавить еще симптомов или причин болезни
 *                        example: false
 *                     isFinalResult:
 *                        type: boolean
 *                        description: Поставлен ли предварительный диагноз
 *                     items:
 *                       type: array
 *                       items:
 *                         allOf:
 *                           - $ref: '#/components/schemas/Desease'
 *                      
 *                       
 */

/**
 * @swagger
 * /deseases/all:
 *   get:
 *     summary: Получить болезни
 *     description: Получить болезни
 *     tags: [DESEASES]
 *     parameters:
 *       - in: query
 *         name: search
 *         schema:
 *           type: string
 *           required: false
 *           description: Поиск по названию болезни
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: array
 *                  items:
 *                     allOf:
 *                        - $ref: '#/components/schemas/Desease'
 *                        - properties:
 *                            id:
 *                              type: string
 *                      
 *                       
 */

/**
 * @swagger
 * /deseases/all/{id}:
 *   get:
 *     summary: Получить болезнь по id
 *     description: Получить болезнь по id
 *     tags: [DESEASES]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           required: true
 *           description: Id сущности
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: object
 *                  properties:
 *                    desease:
 *                      $ref: '#/components/schemas/Desease'
 *             
 */

/**
 * @swagger
 * /deseases/add:
 *   post:
 *     summary: Добавить болезни
 *     description: Добавить болезни
 *     tags: [DESEASES]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               deseases:
 *                  type: array
 *                  items:
 *                    type: object
 *                    properties:
 *                      name:
 *                        type: string
 *                        description: Название болезни.
 *                        example: Сальмонелез
 *                      description:
 *                        type: string
 *                        description: Описание болезни.
 *                        example: Сальмонелез - это ...
 *                      mainDoctor:
 *                        type: string
 *                        description: id
 *                      reasons:
 *                        type: array
 *                        items:
 *                          type: string
 *                          description: id
 *                      symptoms:
 *                        type: array
 *                        items:
 *                          type: string
 *                          description: id
 *                      doctors:
 *                        type: array
 *                        items:
 *                          type: string
 *                          description: id
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: array
 *                  items:
 *                      allOf:
 *                        - $ref: '#/components/schemas/Desease'
 *                        - properties:
 *                            id:
 *                              type: string
 *                      
 *                       
 */

/**
 * @swagger
 * /deseases/all/{id}:
 *   put:
 *     summary: Изменить болезни
 *     description: Изменить болезни
 *     tags: [DESEASES]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           required: true
 *           description: Id симптома
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               reason:
 *                  type: array
 *                  items:
 *                    type: object
 *                    properties:
 *                      name:
 *                        type: string
 *                        description: Название болезни.
 *                        example: Сальмонелез
 *                      description:
 *                        type: string
 *                        description: Описание болезни.
 *                        example: Сальмонелез - это ...
 *                      mainDoctor:
 *                        type: string
 *                        description: id
 *                      reasons:
 *                        type: array
 *                        items:
 *                          type: string
 *                          description: id
 *                      symptoms:
 *                        type: array
 *                        items:
 *                          type: string
 *                          description: id
 *                      doctors:
 *                        type: array
 *                        items:
 *                          type: string
 *                          description: id
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: object
 *                  properties:
 *                    reason:
 *                      allOf:
 *                        - $ref: '#/components/schemas/Desease'
 *                        - properties:
 *                            id:
 *                              type: string
 *         
 */

/**
 * @swagger
 * /deseases/add/{id}:
 *   delete:
 *     summary: Удалить болезни
 *     description: Удалить болезни
 *     tags: [DESEASES]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           required: true
 *           description: Id симптома
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: boolean
 *                  example: true             
 *                       
 */
