const { authJwt, verifySignUp } = require("../middlewares");
const controller = require("../controllers/doctor.controller");
const router = require('express').Router();

router.use(function(req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});

router.get("/all", controller.getDoctors);

router.get("/all/:id", controller.getDoctorById);

router.post("/signup", [
    verifySignUp.checkPasswords,
    verifySignUp.checkDuplicateDoctorEmail,
], controller.signup);

router.post("/signin", controller.signin);

router.get("/me", [authJwt.verifyToken, authJwt.isDoctor], controller.getMe);

router.put("/me", [authJwt.verifyToken, authJwt.isDoctor], controller.changeMe);

module.exports = router;

/**
 * @swagger
 * /doctors/all:
 *   get:
 *     summary: Получить врачей
 *     description: Получить врачей
 *     tags: [DOCTORS]
 *     parameters:
 *       - in: query
 *         name: search
 *         schema:
 *           type: string
 *           required: false
 *           description: Поиск по ФИО врача
 *       - in: query
 *         name: speciality
 *         schema:
 *           type: array
 *           items:
 *              type: string
 *              required: false
 *              description: ID специальности
 *       - in: query
 *         name: clinic
 *         schema:
 *           type: array
 *           items:
 *              type: string
 *              required: false
 *              description: ID клиники
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: array
 *                  items:
 *                    allOf:
 *                      - $ref: '#/components/schemas/Doctor'              
 *                       
 */

/**
 * @swagger
 * /doctors/all/{id}:
 *   get:
 *     summary: Получить врача по id
 *     description: Получить врача по id
 *     tags: [DOCTORS]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           required: true
 *           description: Id сущности
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: object
 *                  properties:
 *                    medicine:
 *                      $ref: '#/components/schemas/Doctor'
 *             
 */

/**
 * @swagger
 * /doctors/me:
 *   get:
 *     summary: Получить врача по токену
 *     description: Получить врача по токену
 *     tags: [DOCTORS]
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: object
 *                  properties:
 *                    medicine:
 *                      $ref: '#/components/schemas/Doctor'
 *             
 */

/**
 * @swagger
 * /doctors/me:
 *   put:
 *     summary: Изменить врача по токену
 *     description: Изменить врача по токену
 *     tags: [DOCTORS]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               medicine:
 *                 allOf:
 *                    - $ref: '#/components/schemas/Doctor'
 *                    - properties:
 *                        clinics:
 *                          type: array
 *                          items:
 *                            type: string
 *                        specialities:
 *                          type: array
 *                          items:
 *                            type: string
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: object
 *                  properties:
 *                    medicine:
 *                      allOf:
 *                        - $ref: '#/components/schemas/Doctor'            
 *                       
 */


/**
 * @swagger
 * /doctors/signup:
 *   post:
 *     summary: Зарегистрировать пользователя.
 *     description: Зарегистрировать пользователя.
 *     tags: [DOCTORS]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *            allOf:
 *              - $ref: '#/components/schemas/Doctor'
 *              - properties:
 *                  clinics:
 *                    type: array
 *                    items:
 *                      type: string
 *                  specialities:
 *                    type: array
 *                    items:
 *                      type: string
 *                  password:
 *                    type: string
 *                    example: QkgsdfU341
 *                  repeatPassword:
 *                    type: string
 *                    example: QkgsdfU341
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              allOf:
 *              - $ref: '#/components/schemas/Doctor'
 *              - properties:
 *                accessToken:
 *                  type: string
 *                       
 */

/**
 * @swagger
 * /doctors/signin:
 *   post:
 *     summary: Авторизовать врача.
 *     description: Авторизовать врача.
 *     tags: [DOCTORS]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *              required:
 *              - email
 *              - password
 *              properties:
 *                 email:
 *                   type: string
 *                   example: ivan@mail.ru
 *                 password:
 *                   type: string
 *                   example: QkgsdfU341
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *             allOf:
 *             - $ref: '#/components/schemas/Doctor'
 *             - properties:
 *                accessToken:
 *                  type: string
 *                       
 */