const { authJwt } = require("../middlewares");
const controller = require("../controllers/speciality.controller");
const router = require('express').Router();

router.use(function(req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});

router.get("/all", controller.getSpeciality);

router.get("/:id", controller.getSpecialityById);

router.post("/add", [authJwt.verifyToken, authJwt.hasSomeRole("admin", "moderator")], controller.postSpeciality);

router.put("/:id", [authJwt.verifyToken, authJwt.hasSomeRole("admin", "moderator")], controller.putSpeciality);

router.delete("/:id", [authJwt.verifyToken, authJwt.hasSomeRole("admin", "moderator")], controller.deleteSpeciality);

module.exports = router;

/**
 * @swagger
 * /speciality/all:
 *   get:
 *     summary: Получить специльности врачей
 *     description: Получить специльности врачей
 *     tags: [SPECIALITY]
 *     parameters:
 *       - in: query
 *         name: search
 *         schema:
 *           type: string
 *           required: false
 *           description: Поиск по названию специальности
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: array
 *                  items:
 *                    allOf:
 *                     - $ref: '#/components/schemas/Speciality'
 *                     - properties:
 *                         id:
 *                           type: string
 *                      
 *                       
 */

/**
 * @swagger
 * /speciality/all/{id}:
 *   get:
 *     summary: Получить специальность по id
 *     description: Получить специальность по id
 *     tags: [SPECIALITY]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           required: true
 *           description: Id сущности
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: object
 *                  properties:
 *                    speciality:
 *                      $ref: '#/components/schemas/Speciality'
 *             
 */

/**
 * @swagger
 * /speciality/add:
 *   post:
 *     summary: Добавить специльности врачей
 *     description: Добавить специльности врачей
 *     tags: [SPECIALITY]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               speciality:
 *                  type: array
 *                  items:
 *                    $ref: '#/components/schemas/Speciality'
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: array
 *                  items:
 *                    allOf:
 *                      - $ref: '#/components/schemas/Speciality'
 *                      - properties:
 *                         id:
 *                           type: string
 *                      
 *                       
 */

/**
 * @swagger
 * /speciality/{id}:
 *   put:
 *     summary: Изменить специльности врачей
 *     description: Изменить специльности врачей
 *     tags: [SPECIALITY]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           required: true
 *           description: Id специальности
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               speciality:
 *                 $ref: '#/components/schemas/Speciality'
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: object
 *                  properties:
 *                    speciality:
 *                      allOf:
 *                        - $ref: '#/components/schemas/Speciality'
 *                        - properties:
 *                            id:
 *                              type: string
 *                      
 *                       
 */

/**
 * @swagger
 * /speciality/{id}:
 *   delete:
 *     summary: Удалить специльности врачей
 *     description: Удалить специльности врачей
 *     tags: [SPECIALITY]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *           required: true
 *           description: Id специальности
 *     responses:
 *      200:
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                message:
 *                  type: string
 *                hasError:
 *                  type: boolean
 *                  description: Есть ли ошибка
 *                  example: false
 *                payload:
 *                  type: boolean
 *                  example: true             
 *                       
 */
