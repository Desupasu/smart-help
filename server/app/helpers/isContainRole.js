const db = require("../models");
const User = db.user;
const Role = db.role;

module.exports.isContainRoles = (req, res, next, rolesArray) => {
    User.findById(req.userId).exec((err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }
  
      Role.find(
        {
          _id: { $in: user.roles }
        },
        (err, roles) => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }
          
          for (let i = 0; i < roles.length; i++) {
            if (rolesArray.includes(roles[i].name)) {
              next();
              return true;
            }
          }
  
          res.status(403).send({ message: `Необходимы права для ${role}` });
          return;
        }
      );
    });
}