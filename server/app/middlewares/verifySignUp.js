const db = require("../models");
const ROLES = db.ROLES;
const User = db.user;
const Doctor = db.doctor;

const checkDuplicatePolicyOrEmail = (req, res, next) => {
  // Username
  User.findOne({
    policy: req.body.policy
  }).exec((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }

    if (user) {
      res.status(400).send({ message: "Пользователь с таким полисом уже зарегистрирован" });
      return;
    }

    // Email
    User.findOne({
      email: req.body.email
    }).exec((err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }

      if (user) {
        res.status(400).send({ message: "Пользователь с таким адресом уже зарегистрирован" });
        return;
      }

      next();
    });
  });
};

const checkDuplicateDoctorEmail = (req, res, next) => {
    Doctor.findOne({
      email: req.body.email
    }).exec((err, user) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }

      if (user) {
        res.status(400).send({ message: "Пользователь с таким адресом уже зарегистрирован" });
        return;
      }

      next();
    });
};

const checkRolesExisted = (req, res, next) => {
  if (req.body.roles) {
    for (let i = 0; i < req.body.roles.length; i++) {
      if (!ROLES.includes(req.body.roles[i])) {
        res.status(400).send({
          message: `Роль ${req.body.roles[i]} не существует`
        });
        return;
      }
    }
  }

  next();
};

const checkPasswords = (req, res, next) => {
  if (req.body.password && req.body.password.length < 8) {
    res.status(400).send({
      message: `Длина пароля должна быть не меньше 8 символов`
    });
    return
  }
  if (req.body.password && !/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/g.test(req.body.password)) {
    res.status(400).send({
      message: `Пароль должен содержать цифры, заглавные и строчные латинские буквы`
    });
    return
  }
  if (req.body.password !== req.body.repeatPassword) {
    res.status(400).send({
      message: `Пароли не совпадают`
    });
    return
  }

  next();
};

const verifySignUp = {
  checkDuplicatePolicyOrEmail,
  checkRolesExisted,
  checkPasswords,
  checkDuplicateDoctorEmail,
};

module.exports = verifySignUp;
