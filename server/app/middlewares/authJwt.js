const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");
const { isContainRoles } = require("../helpers/isContainRole.js");
const db = require("../models");
const User = db.user;
const Doctor = db.doctor;

const verifyToken = (req, res, next) => {
  let token = req.headers["x-access-token"];

  if (!token) {
    return res.status(403).send({ message: "Не указан токен" });
  }

  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({ message: "Необходима авторизация" });
    }
    req.userId = decoded.id;
    next();
  });
};

const isUser = (req, res, next) => {
  User.findById(req.userId).exec((err, user) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }
    if (!user) {
      res.status(403).send({ message: `Необходимы права для пользователя` });
    }
    next();
    return;
  });
};

const isAdmin = (req, res, next) => {
  isContainRoles(req, res, next, ['admin']);
  return;
};

const isModerator = (req, res, next) => {
  isContainRoles(req, res, next, ['moderator']);
  return;
};

const isDoctor = (req, res, next) => {
  Doctor.findById(req.userId).exec((err, doctor) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }
    if (!doctor) {
      res.status(403).send({ message: `Необходимы права для врача` });
    }
    next();
    return;
  });
  return;
};

const isNurse = (req, res, next) => {
  isContainRoles(req, res, next, ['nurse']);
  return;
};

const hasSomeRole = (...roles) => (req, res, next) => {
  isContainRoles(req, res, next, roles);
  return;
}

const authJwt = {
  verifyToken,
  isAdmin,
  isModerator,
  isDoctor,
  isNurse,
  hasSomeRole,
  isUser,
};
module.exports = authJwt;
