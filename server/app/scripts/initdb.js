const db = require("../models");
const specs = require('../../../parser/doctors.json');
const reasons = require('../../../parser/reasons.json');
const symptoms = require('../../../parser/symptoms.json');
const deseases = require('../../../parser/deseases.json');
const medicine = require('../../../parser/medicine.json');
const clinic = require('../../../parser/clinic.json');
const doctors = require('../../../parser/realdoctors2.json');

const bcrypt = require("bcryptjs");

const createUsers = async () => {
  const roles = await db.role.find({});
  return db.user.insertMany([
    {
      "email": "viktor1@mail.com",
      "name": "Виктор",
      "surname": "Власов",
      "patronymic": "Владимирович",
      "policy": "23423523",
      "age": 36,
      "imt": 18,
      "alcoholIndex": 3,
      "smokingIndex": 4,
      "roles": [
        roles.find(item => item.name === 'user')
      ],
      "password": bcrypt.hashSync("Qwerty123", 8),
    },
    {
      "email": "vlad1@mail.com",
      "name": "Влад",
      "surname": "Котов",
      "patronymic": "Михайлович",
      "policy": "764563835",
      "age": 34,
      "imt": 22,
      "alcoholIndex": 2,
      "smokingIndex": 1,
      "roles": [
        roles.find(item => item.name === 'user')
      ],
      "password": bcrypt.hashSync("Qwerty123", 8),
    },
    {
      "email": "admin@mail.com",
      "name": "Админ",
      "surname": "Админов",
      "patronymic": "Админович",
      "policy": "0000000000",
      "age": 50,
      "imt": 22,
      "alcoholIndex": 1,
      "smokingIndex": 1,
      "roles": [
        roles.find(item => item.name === 'admin')
      ],
      "password": bcrypt.hashSync("Qwerty123", 8),
    },
  ])
}

const createSpecialities = async () => {
    return db.speciality.insertMany(specs.map(item => ({name: item})));
}

const createReasons = async () => {
    return db.reason.insertMany(reasons.map(item => ({name: item.toLowerCase()})));
}

const createSymptoms = async () => {
    return db.symptom.insertMany(symptoms.map(item => ({name: item.toLowerCase() })));
}

const createMedicine = async () => {
  return db.medicine.insertMany(medicine.map(item => ({name: item.name.toLowerCase() })));
}

const createClinic = async () => {
  return db.clinic.insertMany(clinic.map((item, index) => ({
    name: item.name,
    address: item.address,
    workTime: item.workTime,
    type: index % 4 === 0 ? "Клиника" : "Медицинский центр"
  })));
}


const createDoctors = async () => {

  for (item of doctors) {
      const findSpeciality = await db.speciality.find({ name: {
          $in: item.specialities
      }
      });
      await new db.doctor({
          name: item.name,
          surname: item.surname,
          lastName: item.lastName,
          startWork: item.startWork,
          email: item.email,
          password: bcrypt.hashSync(item.password, 8),
          specialities: findSpeciality.map(n => n._id),
      }).save();
  }
}

const createDeseases = async () => {

    for (item of deseases) {
        const findSymptoms = await db.symptom.find({ name: {
            $in: item.symptoms.map(n => n.toLowerCase())
        }
        });
        const findDoctors = await db.speciality.find({ name: {
            $in: item.doctors
        }
        });
        const findDoctor = await db.speciality.findOne({ name: item.mainDoctor});
        const findReasons = await db.reason.find({ name: {
            $in: item.reasons.map(n => n.toLowerCase())
        }
        });
        await new db.desease({
            name: item.name,
            description: item.description,
            mainDoctor: findDoctor._id,
            reasons: findReasons.map(n => n._id),
            symptoms: findSymptoms.map(n => n._id),
            doctors: findDoctors.map(n => n._id)
        }).save();
    }
}

const createRoles = async () => {
    const Role = db.role;
    return db.role.insertMany( [
        new Role({
          name: "user"
        }),
        new Role({
          name: "doctor"
        }),
        new Role({
          name: "nurse"
        }),
        new Role({
          name: "admin"
        }),
        new Role({
          name: "moderator"
        })
      ]);
}

const createDoctorClinicSlots = async () => {
  let index = 0;
  for (item of doctors) {
    index++;
    const findDoctor = await db.doctor.findOne({ email: item.email });
    const findClinic = await db.clinic.findOne({ name: {
        $in: item.clinics
    }
    });
    const doctorClinicId = await db.doctorClinic.create({
        doctor: findDoctor._id,
        clinic: findClinic._id,
        canOnline: index % 2 === 0
    });
    if (item.meets && item.meets.length) {
      await db.slot.insertMany(item.meets.map(n => ({
        doctorClinic: doctorClinicId._id,
        isOnline: index % 2 === 0,
        isActive: true,
        stDate: n.st_date,
        endDate: n.end_date
      })))
    }
  }
}

module.exports = {
    createSpecialities,
    createRoles,
    createReasons,
    createSymptoms,
    createDeseases,
    createMedicine,
    createClinic,
    createUsers,
    createDoctors,
    createDoctorClinicSlots,
}