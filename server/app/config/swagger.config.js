module.exports = {
    definition: {
        openapi: "3.0.0",
        info: {
        title: "Smart Help Api",
        version: "0.1.0",
        description:
            "This is a Smart Help API",
        license: {
            name: "MIT",
            url: "https://spdx.org/licenses/MIT.html",
        },
        },
        servers: [
        {
            url: "https://salty-cove-00608.herokuapp.com",
        },
        {
            url: "http://localhost:8080",
        },
        ],
    },
    apis: ['./server/app/**/*.js'],
    };