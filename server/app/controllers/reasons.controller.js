const db = require("../models");
const Reason = db.reason;

exports.getReasons = async (req, res) => {
    const search = req.query.search || '';
    try {
        const response = await Reason.find({ name: { '$regex': search, "$options": "ig" }}).sort({ name: 1 });
        res.status(200).send({
            message: '',
            hasError: false,
            payload: response.map(item => ({
                name: item.name,
                description: item.description,
                id: item._id
            }))
        });
    } catch(err) {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
    }
};

exports.getReasonById = async (req, res) => {
    const id = req.params.id || '';
    try {
        const item = await Reason.findById(id);
        res.status(200).send({
            message: '',
            hasError: false,
            payload: {
                name: item.name,
                description: item.description,
                id: item._id
            }
        });
    } catch(err) {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
    }
};

exports.postReasons = async (req, res) => {
    const reasons = req.body.reasons;
    if (!Array.isArray(reasons)) {
            res.status(500).send({
              message: 'reasons не является массивом',
              hasError: true,
              payload: null
            });
            return;
    }
    if (reasons.some(item => !item.name)) {
        res.status(500).send({
          message: 'Название обязательно при добавлении причины болезни',
          hasError: true,
          payload: null
        });
        return;
}
    Reason.insertMany(reasons, (err, response) => {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
        res.status(200).send({
            message: '',
            hasError: false,
            payload: response.map(item => ({
                name: item.name,
                description: item.description,
                id: item._id
            }))
        });
    });
};

exports.putReasons = async (req, res) => {
    const id = req.params.id;
    const current = req.body.reason;
    Reason.findByIdAndUpdate(id, { ...current }, (err, reason) => {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
        res.status(200).send({
            message: '',
            hasError: false,
            payload: {
                name: reason.name,
                description: reason.description,
                id: reason._id
            }
        });
    })
};

exports.deleteReasons = async (req, res) => {
    const id = req.params.id;
    Reason.findByIdAndRemove(id, (err, reason) => {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
        res.status(200).send({
            message: '',
            hasError: false,
            payload: true
        });
    })
};