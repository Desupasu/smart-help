const db = require("../models");
const Symptom = db.symptom;

exports.getSymptoms = async (req, res) => {
    const search = req.query.search || '';
    try {
        let response;
        if (search) {
            response = await Symptom.find({ name: { '$regex': search.split(' ').map(n => `${n}?.?`).join(' '), "$options": "ig" }}).sort({ name: 1 });
        } else {
            response = await Symptom.find({}).sort({ name: 1 });
        }
        res.status(200).send({
            message: '',
            hasError: false,
            payload: response.map(item => ({
                name: item.name,
                description: item.description,
                id: item._id
            }))
        });
    } catch(err) {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
    }
};


exports.getSymptomById = async (req, res) => {
    const id = req.params.id || '';
    try {
        const item = await Symptom.findById(id);
        res.status(200).send({
            message: '',
            hasError: false,
            payload: {
                name: item.name,
                description: item.description,
                id: item._id
            }
        });
    } catch(err) {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
    }
};


exports.postSymptoms = async (req, res) => {
    const symptoms = req.body.symptoms;
    if (!Array.isArray(symptoms)) {
            res.status(500).send({
              message: 'symptoms не является массивом',
              hasError: true,
              payload: null
            });
            return;
    }
    if (symptoms.some(item => !item.name)) {
        res.status(500).send({
          message: 'Название обязательно при добавлении симптома болезни',
          hasError: true,
          payload: null
        });
        return;
    }  
    Symptom.insertMany(symptoms, (err, response) => {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
        res.status(200).send({
            message: '',
            hasError: false,
            payload: response.map(item => ({
                name: item.name,
                description: item.description,
                id: item._id
            }))
        });
    });
};

exports.putSymptoms = async (req, res) => {
    const id = req.params.id;
    const current = req.body.symptom;
    Symptom.findByIdAndUpdate(id, { ...current }, (err, symptom) => {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
        res.status(200).send({
            message: '',
            hasError: false,
            payload: {
                name: symptom.name,
                description: symptom.description,
                id: symptom._id
            }
        });
    })
};

exports.deleteSymptoms = async (req, res) => {
    const id = req.params.id;
    Symptom.findByIdAndRemove(id, (err, symptom) => {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
        res.status(200).send({
            message: '',
            hasError: false,
            payload: true
        });
    })
};