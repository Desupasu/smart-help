const { mongoose } = require("../models");
const db = require("../models");
const Slot = db.slot;
const DoctorClinic = db.doctorClinic;

exports.getAllClinicSlots = async (req, res) => {
    const clinic = req.query.clinic || '';
    const doctor = req.query.doctor || '';
    const st_date = req.query.st_date || '';
    const end_date = req.query.end_date || '';
    const query = { isActive: true, isOnline: false };
    const doctorClinicQuery = {};
    query.$and = [{ 
        stDate: {
            $gte: st_date
        },
    }, { 
        endDate: {
            $lte: end_date
        },
    }]
    try {
        if (doctor) doctorClinicQuery.doctor = doctor;
        if (clinic) doctorClinicQuery.clinic = clinic;
        if (doctor || clinic) {
            const findDoctorClinic = await DoctorClinic.find(doctorClinicQuery);
            query.doctorClinic = { $in: findDoctorClinic.map(n => mongoose.Types.ObjectId(n._id))};
        }
        const response = await Slot.aggregate([
            {$match: query},
            {
                $lookup: {
                  from: "doctorclinics",
                  localField: "doctorClinic",
                  foreignField: "_id",
                  as: "doctorclinics"
                },
            },
            { $unwind: "$doctorclinics" },
            {
                $lookup: {
                    from: "doctors",
                    localField: "doctorclinics.doctor",
                    foreignField: "_id",
                    as: "doctors"
                },
            },
            { $unwind: "$doctors" },
            {
                $lookup: {
                    from: "clinics",
                    localField: "doctorclinics.clinic",
                    foreignField: "_id",
                    as: "clinics"
                },
            },
            { $unwind: "$clinics" },
            {$group:
             {
               _id: {
                doctor: "$doctorclinics.doctor",
                clinic: "$doctorclinics.clinic",
               },
               doctor: { $first: {
                   _id: "$doctors._id",
                   name: "$doctors.name",
                   surname: "$doctors.surname",
                   lastName: "$doctors.lastName",
                   startWork: "$doctors.startWork",
                }},
                clinic: { $first: {
                    name: "$clinics.name",
                    address: "$clinics.address",
                    workTime: "$clinics.workTime"
                }},
               slots: { $addToSet: {
                stDate: "$stDate",
                endDate: "$endDate",
                isActive: "$isActive",
                slotId: "$_id"
               }}
             }
            },
            {$group:
                {
                  _id: "$_id.doctor",
                  doctor: { $push: "$doctor" },
                  items: { $push: {
                    _id: "$_id.clinic",
                    clinic: "$clinic",
                    slots: "$slots"
                  }}
                }
            },
            { $project: {
                _id: 0,
                doctor: 1,
                items: 1,
            }}
        ]);
        res.status(200).send({
            message: '',
            hasError: false,
            payload: response
        });
    } catch(err) {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
    }
};

exports.getAllOnlineSlots = async (req, res) => {
    const clinic = req.query.clinic || '';
    const doctor = req.query.doctor || '';
    const st_date = req.query.st_date || '';
    const end_date = req.query.end_date || '';
    const query = { isActive: true, isOnline: true };
    const doctorClinicQuery = {};
    query.$and = [{ 
        stDate: {
            $gte: st_date
        },
    }, { 
        endDate: {
            $lte: end_date
        },
    }]
    try {
        if (doctor) doctorClinicQuery.doctor = doctor;
        if (clinic) doctorClinicQuery.clinic = clinic;
        if (doctor || clinic) {
            const findDoctorClinic = await DoctorClinic.find(doctorClinicQuery);
            query.doctorClinic = { $in: findDoctorClinic.map(n => mongoose.Types.ObjectId(n._id))};
        }
        const response = await Slot.aggregate([
            {$match: query},
            {
                $lookup: {
                  from: "doctorclinics",
                  localField: "doctorClinic",
                  foreignField: "_id",
                  as: "doctorclinics"
                },
            },
            { $unwind: "$doctorclinics" },
            {
                $lookup: {
                    from: "doctors",
                    localField: "doctorclinics.doctor",
                    foreignField: "_id",
                    as: "doctors"
                },
            },
            { $unwind: "$doctors" },
            {
                $lookup: {
                    from: "clinics",
                    localField: "doctorclinics.clinic",
                    foreignField: "_id",
                    as: "clinics"
                },
            },
            { $unwind: "$clinics" },
            {$group:
             {
               _id: {
                doctor: "$doctorclinics.doctor",
                clinic: "$doctorclinics.clinic",
               },
               doctor: { $first: {
                   _id: "$doctors._id",
                   name: "$doctors.name",
                   surname: "$doctors.surname",
                   lastName: "$doctors.lastName",
                   startWork: "$doctors.startWork",
                }},
                clinic: { $first: {
                    name: "$clinics.name",
                    address: "$clinics.address",
                    workTime: "$clinics.workTime"
                }},
               slots: { $addToSet: {
                stDate: "$stDate",
                endDate: "$endDate",
                isActive: "$isActive",
                slotId: "$_id"
               }}
             }
            },
            {$group:
                {
                  _id: "$_id.doctor",
                  doctor: { $push: "$doctor" },
                  items: { $push: {
                    _id: "$_id.clinic",
                    clinic: "$clinic",
                    slots: "$slots"
                  }}
                }
            },
            { $project: {
                _id: 0,
                doctor: 1,
                items: 1,
            }}
        ]);
        res.status(200).send({
            message: '',
            hasError: false,
            payload: response
        });
    } catch(err) {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
    }
};