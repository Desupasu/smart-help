const { mongoose } = require("../models");
const db = require("../models");
const Clinic = db.clinic;
const Doctor = db.doctor;
const DoctorClinic = db.doctorClinic;

exports.getClinics = async (req, res) => {
    const search = req.query.search || '';
    const speciality = req.query.speciality || '';
    const query = {}
    if (search) {
        query.name = { '$regex': search, "$options": "ig" };
    }
    if (speciality) {
        const doctors = await Doctor.find({ specialities: { $in: speciality }});
        let findClinics = await DoctorClinic.find({ doctor: { $in: doctors.map(item => mongoose.Types.ObjectId(item._id)) }}).distinct("clinic");
        query._id = findClinics.map(item => mongoose.Types.ObjectId(item._id));
    }
    try {
        const response = await Clinic.find(query).sort({ name: 1 });
        res.status(200).send({
            message: '',
            hasError: false,
            payload: response.map(item => ({
                name: item.name,
                address: item.address,
                workTime: item.workTime,
                id: item._id
            }))
        });
    } catch(err) {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
    }
};

exports.getClinicById = async (req, res) => {
    const id = req.params.id || '';
    try {
        const item = await Clinic.findById(id);
        res.status(200).send({
            message: '',
            hasError: false,
            payload: {
                name: item.name,
                address: item.address,
                workTime: item.workTime,
                id: item._id
            }});
    } catch(err) {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
    }
};

exports.postClinics = async (req, res) => {
    const clinics = req.body.clinics;
    if (!Array.isArray(clinics)) {
            res.status(500).send({
              message: 'clinics не является массивом',
              hasError: true,
              payload: null
            });
            return;
    }
    if (clinics.some(item => !item.name)) {
        res.status(500).send({
          message: 'Название обязательно при добавлении клиник',
          hasError: true,
          payload: null
        });
        return;
}
    Clinic.insertMany(clinics, (err, response) => {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
        res.status(200).send({
            message: '',
            hasError: false,
            payload: response.map(item => ({
                name: item.name,
                address: item.address,
                workTime: item.workTime,
                id: item._id
            }))
        });
    });
};

exports.putClinics = async (req, res) => {
    const id = req.params.id;
    const current = req.body.clinic;
    Clinic.findByIdAndUpdate(id, { ...current }, (err, clinic) => {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
        res.status(200).send({
            message: '',
            hasError: false,
            payload: {
                name: item.name,
                address: item.address,
                workTime: item.workTime,
                id: item._id
            }
        });
    })
};

exports.deleteClinics = async (req, res) => {
    const id = req.params.id;
    Clinic.findByIdAndRemove(id, (err, clinic) => {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
        res.status(200).send({
            message: '',
            hasError: false,
            payload: true
        });
    })
};