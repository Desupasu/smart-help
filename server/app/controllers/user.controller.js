const db = require("../models");
const mongoose = require('mongoose');
const User = db.user;
const Meet = db.meet;
const Slot = db.slot;

exports.getMe = (req, res) => {
  User.findById(req.userId, async (err, user) => {

    if (err) {
      res.status(500).send({
        message: err.message,
        hasError: true,
        payload: null
      });
      return;
    }

    res.status(200).send({
      message: '',
      hasError: false,
      payload: {
        email: user.email,
        name: user.name,
        surname: user.surname,
        patronymic: user.patronymic,
        policy: user.policy,
        age: user.age,
        imt: user.imt,
        alcoholIndex: user.alcoholIndex,
        smokingIndex: user.smokingIndex,
        roles: ["ROLE_USER"],
        gender: user.gender,
      }
    });
  })
}

exports.changeMe = (req, res) => {
  User.findByIdAndUpdate(req.userId, {
    imt: req.body.imt, age: req.body.age, alcoholIndex: req.body.alcoholIndex, smokingIndex: req.body.smokingIndex, gender: req.body.gender, chronicDeseases, usedMedicine
  }, {useFindAndModify: false}, (err, user) => {

    if (err) {
      res.status(500).send({
        message: err.message,
        hasError: true,
        payload: null
      });
      return;
    }

    res.status(200).send({
      message: '',
      hasError: false,
      payload: {
        email: user.email,
        name: user.name,
        surname: user.surname,
        patronymic: user.patronymic,
        policy: user.policy,
        age: user.age,
        imt: user.imt,
        alcoholIndex: user.alcoholIndex,
        smokingIndex: user.smokingIndex,
        roles: ["ROLE_USER"],
        gender: user.gender,
      }
    });
  })
}

exports.getMeets = async (req, res) => {
  const userId = req.userId;
  try {
  const result = await Meet.find({ user: mongoose.Types.ObjectId(userId) }).populate({ path: "slot symptoms speciality", populate: { path: 'doctorClinic', populate: "doctor clinic" }, select: "name description endDate stDate isActive isOnline doctorClinic"})
  .populate({ path: "doctorClinic" }).populate({ path: "doctor clinic" });
  res.status(200).send({
      message: '',
      hasError: false,
      payload: result.map(item => ({
          symptoms: item.symptoms,
          speciality: item.speciality,
          doctor: item.slot.doctorClinic.doctor,
          clinic: item.slot.doctorClinic.clinic,
          stDate: item.slot.stDate,
          endDate: item.slot.endDate,
          isOnline: item.slot.isOnline,
      }))
  });
  } catch(err) {
      if (err) {
          res.status(500).send({
            message: err.message,
            hasError: true,
            payload: null
          });
          return;
      }
  }
};

exports.applyMeet = async (req, res) => {
  const meet = req.body.meet;
  const userId = req.userId;
  try {
  const slot = await Slot.findById({...meet.slot, user: userId });
  if (!slot.isActive) {
      res.status(500).send({
          message: 'Это время уже занято',
          hasError: true,
          payload: null
      });
      return;
  }
  await Slot.findByIdAndUpdate(meet.slot, { isActive: false });
  const savedMeet = await Meet.create(meet);
  const item = await savedMeet.populate({ path: "slot symptoms speciality", populate: { path: 'doctorClinic', populate: "doctor clinic" }, select: "name description endDate stDate isActive isOnline doctorClinic"})
  .populate({ path: "doctorClinic" }).populate({ path: "doctor clinic" }).execPopulate();
  res.status(200).send({
      message: '',
      hasError: false,
      payload: {
          symptoms: item.symptoms,
          speciality: item.speciality,
          doctor: item.slot.doctorClinic.doctor,
          clinic: item.slot.doctorClinic.clinic,
          stDate: item.slot.stDate,
          endDate: item.slot.endDate,
          isOnline: item.slot.isOnline,
      }
  });
  } catch(err) {
      if (err) {
          res.status(500).send({
            message: err.message,
            hasError: true,
            payload: null
          });
          return;
      }
  }
};