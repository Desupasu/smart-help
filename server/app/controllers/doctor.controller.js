const db = require("../models");
const Doctor = db.doctor;
const DoctorClinic = db.doctorClinic;
const mongoose = require('mongoose');
const config = require("../config/auth.config");

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

exports.getDoctors = async (req, res) => {
    let search = req.query.search || '';
    let speciality = req.query.speciality || '';
    let clinic = req.query.clinic || '';
    try {
        const query = {};
        if (search) {
            const re = new RegExp(search, 'ig');
            const findDoctors = await Doctor.aggregate()
            .project({fullName: {$concat: ['$surname', ' ', '$name' , ' ', '$lastName']}})
            .match({fullName: re});
            query._id = { $in: findDoctors.map(item => mongoose.Types.ObjectId(item._id)) };
        }
        if (speciality) {
            if (speciality && !Array.isArray(speciality)) {
                speciality = [speciality];
            }
            query.specialities = { $in: speciality };
        }
        if (clinic) {
            if (clinic && !Array.isArray(clinic)) {
                clinic = [clinic];
            }
            const clinicQuery = { clinic: { $in: clinic} };
            if (search) clinicQuery.doctor = query._id;
            const findDoctorClinics = await DoctorClinic.find(clinicQuery);
            query._id = { $in: findDoctorClinics.map(item => mongoose.Types.ObjectId(item.doctor)) };
        }
        const doctors = await Doctor.find(query).sort({ name: 1 }).populate({ path: "specialities", select: "name description"});
        res.status(200).send({
            message: '',
            hasError: false,
            payload: doctors
        });
    } catch(err) {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
    }
};

exports.getDoctorById = async (req, res) => {
    const id = req.params.id || '';
    try {
        const item = await Doctor.findById(id).populate({ path: "specialities", select: "name description"});
        res.status(200).send({
            message: '',
            hasError: false,
            payload: item
        });
    } catch(err) {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
    }
};


exports.signup = (req, res) => {
    const doctor = new Doctor({
        name: req.body.name,
        surname: req.body.surname,
        lastName: req.body.lastName,
        startWork: req.body.startWork,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 8),
        specialities: req.body.specialities,
    });
  
    doctor.save((err, doctor) => {
      if (err) {
        res.status(500).send({ 
          message: err.message,
          hasError: true,
          payload: null
        });
        return;
      } 
  
        let token = jwt.sign({ id: doctor.id }, config.secret, {
            expiresIn: 86400 // 24 hours
        });

        doctor
        .populate({ path: "specialities", select: "name description"})
        .execPopulate((err, doctor) => {
            res.status(200).send({
                message: '',
                hasError: false,
                payload: {
                    name: doctor.name,
                    surname: doctor.surname,
                    lastName: doctor.lastName,
                    startWork: doctor.startWork,
                    email: doctor.email,
                    specialities: doctor.specialities,
                    roles: ["ROLE_DOCTOR"],
                    accessToken: token
                }
            })
        })
    });
};
  
  exports.signin = (req, res) => {
    let doctor = null;
    if (req.body.email) {
      doctor = Doctor.findOne({
        email: req.body.email
      })
    }
    if (!doctor) {
      res.status(404).send({
        message: "Врач не найден",
        hasError: true,
        payload: null
      });
      return;
    }
    doctor
      .populate({ path: "specialities", select: "name description"})
      .exec((err, doctor) => {
        if (err) {
          res.status(500).send({
            message: err.message,
            hasError: true,
            payload: null
          });
          return;
        }
  
        if (!doctor) {
          return res.status(404).send({
            message: "Врач не найден",
            hasError: true,
            payload: null
          });
        }
  
        let passwordIsValid = bcrypt.compareSync(
          req.body.password,
          doctor.password
        );
  
        if (!passwordIsValid) {
          return res.status(401).send({
            message: "Неверный пароль",
            hasError: true,
            payload: null
            
          });
        }
        let token = jwt.sign({ id: doctor.id }, config.secret, {
            expiresIn: 86400 // 24 hours
        });

        res.status(200).send({
            name: doctor.name,
            surname: doctor.surname,
            lastName: doctor.lastName,
            startWork: doctor.startWork,
            email: doctor.email,
            specialities: doctor.specialities,
            roles: ["ROLE_DOCTOR"],
            accessToken: token
        })
      });
  };
  

  exports.getMe = (req, res) => {
    Doctor.findById(req.userId, async (err, doctor) => {
  
      if (err) {
        res.status(500).send({
          message: err.message,
          hasError: true,
          payload: null
        });
        return;
      }
      doctor
        .populate({ path: "specialities", select: "name description"})
        .execPopulate((err, doctor) => {
            res.status(200).send({
                message: '',
                hasError: false,
                payload: {
                    name: doctor.name,
                    surname: doctor.surname,
                    lastName: doctor.lastName,
                    startWork: doctor.startWork,
                    email: doctor.email,
                    specialities: doctor.specialities,
                    roles: ["ROLE_DOCTOR"],
                }
            });
        })
    })
  }
  
  exports.changeMe = (req, res) => {
    Doctor.findByIdAndUpdate(req.userId, {
        startWork: req.body.startWork,
        specialities: req.body.specialities,
    }, {useFindAndModify: false}, (err, user) => {
  
      if (err) {
        res.status(500).send({
          message: err.message,
          hasError: true,
          payload: null
        });
        return;
      }
  
      doctor
        .populate({ path: "specialities", select: "name description"})
        .execPopulate((err, doctor) => {
            res.status(200).send({
                message: '',
                hasError: false,
                payload: {
                    name: doctor.name,
                    surname: doctor.surname,
                    lastName: doctor.lastName,
                    startWork: doctor.startWork,
                    email: doctor.email,
                    specialities: doctor.specialities,
                    roles: ["ROLE_DOCTOR"],
                }
            });
        })
    })
  }