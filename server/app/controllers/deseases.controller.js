const db = require("../models");
const Desease = db.desease;
const mongoose = require('mongoose');

exports.getDeseasesBySymptomsAndReasons = async (req, res) => {
    const symptoms = req.query.symptoms || [];
    const reasons = req.query.reasons || [];
    const result = await Desease.aggregate([
        {$project:{
            doctor: "$mainDoctor",
            symptomCount: {$size: {$setIntersection: [ "$symptoms", symptoms.map(item => mongoose.Types.ObjectId(item)) ]} },
            reasonCount:  {$size: {$setIntersection: [ "$reasons", reasons.map(item => mongoose.Types.ObjectId(item)) ]} },
            intersectCount : { $add : [ {$size: {$setIntersection: [ "$reasons", reasons.map(item => mongoose.Types.ObjectId(item)) ]} }, {$size: {$setIntersection: [ "$symptoms", symptoms.map(item => mongoose.Types.ObjectId(item)) ]} } ] },
        }},
        {$match: {$expr: {$gt: [{$sum: [ "$symptomCount", "$reasonCount" ]}, 1] }}},
        {$group:
         {
           _id: "$doctor",
           totalAmount: { $max: { $add: [ "$symptomCount", "$reasonCount" ] } },
           deseases: { $addToSet: "$_id" }
         }
        },
        { $sort: { totalAmount: -1 }}
    ]);

    let resultDeseases = [];
    let doctorCount = 0;

    for (let i = 0; i < result.length; i++) {
        if (result[i - 1] && result[i].totalAmount < result[i - 1].totalAmount) {
            break;
        }
        doctorCount++;
        resultDeseases = resultDeseases.concat(result[i].deseases);
    }

    const formatMessage = (n) => {
        if (n === 0 || n >= 5) {
            return 'Добавьте больше симптомов'
        } else if (n > 2 && n < 5) {
            return 'Добавьте больше симптомов или выберите причины болезней, которые можете отнести к себе'
        }
        return '';
    }

    Desease.find({ _id: { $in: resultDeseases.map(item => mongoose.Types.ObjectId(item)) }}).populate({ path: "doctors reasons symptoms mainDoctor", select: "name description" }).exec((err, response) => {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        };
        res.status(200).send({
            message: formatMessage(resultDeseases.length),
            hasError: false,
            payload: {
                isAddSymptom: resultDeseases.length === 0 || resultDeseases.length >= 5,
                isAddReasonOrSymptom: resultDeseases.length > 2 && resultDeseases.length < 5,
                isFinalResult: doctorCount === 1 && resultDeseases.length <= 2,
                items: response
            }
        });
    })
};

exports.getDeseases = async (req, res) => {
    const search = req.query.search || '';
    Desease.find({ name: { '$regex': search, "$options": "ig" }}).sort({ name: 1 }).populate({ path: "doctors reasons symptoms mainDoctor", select: "name description" }).exec((err, response) => {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
        res.status(200).send({
            message: '',
            hasError: false,
            payload: response
        });
    });
};

exports.getDeseaseById = async (req, res) => {
    const id = req.params.id || '';
    try {
        const item = await Desease.findById(id).populate({ path: "doctors reasons symptoms mainDoctor", select: "name description" });
        res.status(200).send({
            message: '',
            hasError: false,
            payload: item
        });
    } catch(err) {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
    }
};

exports.postDeseases = async (req, res) => {
    const deseases = req.body.deseases;
    if (!Array.isArray(deseases)) {
            res.status(500).send({
              message: 'deseases не является массивом',
              hasError: true,
              payload: null
            });
            return;
    }
    if (deseases.some(item => !item.name)) {
        res.status(500).send({
          message: 'Название обязательно при добавлении причины болезни',
          hasError: true,
          payload: null
        });
        return;
    }  
    Desease.insertMany(deseases, async (err, response) => {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
        Desease.find({ _id: { $in: response.map(item => mongoose.Types.ObjectId(item._id)) } }).sort({ name: 1 }).populate({ path: "doctors reasons symptoms mainDoctor", select: "name description" }).exec((err, response) => {
            if (err) {
                res.status(500).send({
                  message: err.message,
                  hasError: true,
                  payload: null
                });
                return;
            }
            res.status(200).send({
                message: '',
                hasError: false,
                payload: response
            });
        })
    });
};

exports.putDeseases = async (req, res) => {
    const id = req.params.id;
    const current = req.body.desease;
    Desease.findByIdAndUpdate(id, { ...current }, (err, desease) => {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
        Desease.findById(desease._id).populate({ path: "doctors reasons symptoms mainDoctor", select: "name description" }).exec((err, response) => {
            if (err) {
                res.status(500).send({
                  message: err.message,
                  hasError: true,
                  payload: null
                });
                return;
            }
            res.status(200).send({
                message: '',
                hasError: false,
                payload: response
            });
        });
    })
};

exports.deleteDeseases = async (req, res) => {
    const id = req.params.id;
    Desease.findByIdAndRemove(id, (err, desease) => {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
        res.status(200).send({
            message: '',
            hasError: false,
            payload: true
        });
    })
};