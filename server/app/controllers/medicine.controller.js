const db = require("../models");
const Medicine = db.medicine;

exports.getMedicines = async (req, res) => {
    const search = req.query.search || '';
    try {
        const response = await Medicine.find({ name: { '$regex': search, "$options": "ig" }}).sort({ name: 1 });
        res.status(200).send({
            message: '',
            hasError: false,
            payload: response.map(item => ({
                name: item.name,
                description: item.description,
                id: item._id
            }))
        });
    } catch(err) {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
    }
};

exports.getMedicineById = async (req, res) => {
    const id = req.params.id || '';
    try {
        const item = await Desease.findById(id);
        res.status(200).send({
            message: '',
            hasError: false,
            payload: {
                name: item.name,
                description: item.description,
                id: item._id
            }
        });
    } catch(err) {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
    }
};

exports.postMedicines = async (req, res) => {
    const medicines = req.body.medicines;
    if (!Array.isArray(medicines)) {
            res.status(500).send({
              message: 'medicines не является массивом',
              hasError: true,
              payload: null
            });
            return;
    }
    if (medicines.some(item => !item.name)) {
        res.status(500).send({
          message: 'Название обязательно при добавлении препаратов',
          hasError: true,
          payload: null
        });
        return;
}
    Medicine.insertMany(medicines, (err, response) => {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
        res.status(200).send({
            message: '',
            hasError: false,
            payload: response.map(item => ({
                name: item.name,
                description: item.description,
                id: item._id
            }))
        });
    });
};

exports.putMedicines = async (req, res) => {
    const id = req.params.id;
    const current = req.body.medicine;
    Medicine.findByIdAndUpdate(id, { ...current }, (err, medicine) => {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
        res.status(200).send({
            message: '',
            hasError: false,
            payload: {
                name: medicine.name,
                description: medicine.description,
                id: medicine._id
            }
        });
    })
};

exports.deleteMedicines = async (req, res) => {
    const id = req.params.id;
    Medicine.findByIdAndRemove(id, (err, medicine) => {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
        res.status(200).send({
            message: '',
            hasError: false,
            payload: true
        });
    })
};