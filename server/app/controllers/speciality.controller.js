const db = require("../models");
const Speciality = db.speciality;

exports.getSpeciality = async (req, res) => {
    const search = req.query.search || '';
    try {
        const response = await Speciality.find({ name: { '$regex': search, "$options": "ig" }}).sort({ name: 1 });
        res.status(200).send({
            message: '',
            hasError: false,
            payload: response.map(item => ({
                name: item.name,
                description: item.description,
                id: item._id
            }))
        });
    } catch(err) {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
    }
};

exports.getSpecialityById = async (req, res) => {
    const id = req.params.id || '';
    try {
        const item = await Speciality.findById(id);
        res.status(200).send({
            message: '',
            hasError: false,
            payload: {
                name: item.name,
                description: item.description,
                id: item._id
            }
        });
    } catch(err) {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
    }
};

exports.postSpeciality = async (req, res) => {
    const speciality = req.body.speciality;
    if (!Array.isArray(speciality)) {
            res.status(500).send({
              message: 'speciality не является массивом',
              hasError: true,
              payload: null
            });
            return;
    }
    if (speciality.some(item => !item.name)) {
        res.status(500).send({
          message: 'Название обязательно при добавлении специальности',
          hasError: true,
          payload: null
        });
        return;
    }  
    Speciality.insertMany(speciality, (err, response) => {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
        res.status(200).send({
            message: '',
            hasError: false,
            payload: response.map(item => ({
                name: item.name,
                description: item.description,
                id: item._id
            }))
        });
    });
};

exports.putSpeciality = async (req, res) => {
    const id = req.params.id;
    const current = req.body.speciality;
    Speciality.findByIdAndUpdate(id, { ...current }, (err, speciality) => {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
        res.status(200).send({
            message: '',
            hasError: false,
            payload: {
                name: speciality.name,
                description: speciality.description,
                id: speciality._id
            }
        });
    })
};

exports.deleteSpeciality = async (req, res) => {
    const id = req.params.id;
    Speciality.findByIdAndRemove(id, (err, speciality) => {
        if (err) {
            res.status(500).send({
              message: err.message,
              hasError: true,
              payload: null
            });
            return;
        }
        res.status(200).send({
            message: '',
            hasError: false,
            payload: true
        });
    })
};