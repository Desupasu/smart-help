const config = require("../config/auth.config");
const db = require("../models");
const User = db.user;
const Role = db.role;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

exports.signup = (req, res) => {
  const user = new User({
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8),
    name: req.body.name,
    surname: req.body.surname,
    patronymic: req.body.patronymic,
    policy: req.body.policy,
    age: req.body.age ?? null,
    imt: req.body.imt ?? null,
    alcoholIndex: req.body.alcoholIndex ?? null,
    smokingIndex: req.body.smokingIndex ?? null,
  });

  user.save((err, user) => {
    if (err) {
      res.status(500).send({ 
        message: err.message,
        hasError: true,
        payload: null
      });
      return;
    } 
    Role.findOne({ name: "user" }, (err, role) => {
      if (err) {
        res.status(500).send({
          message: err.message,
          hasError: true,
          payload: null
        });
        return;
      }

      user.roles = [role._id];
      user.save((err, user) => {
        if (err) {
          res.status(500).send({
            message: err.message,
            hasError: true,
            payload: null
          });
          return;
        }

        let token = jwt.sign({ id: user.id }, config.secret, {
          expiresIn: 86400 // 24 hours
        });
  
        let authorities = ["ROLE_" + role.name.toUpperCase()];

        res.status(200).send({
          message: '',
          hasError: false,
          payload: {
            email: user.email,
            name: user.name,
            surname: user.surname,
            patronymic: user.patronymic,
            policy: user.policy,
            age: user.age,
            imt: user.imt,
            alcoholIndex: user.alcoholIndex,
            smokingIndex: user.smokingIndex,
            gender: user.gender,
            roles: authorities,
            accessToken: token
          }
        });
      });
    });
  });
};

exports.signin = (req, res) => {
  let user = null;
  if (req.body.email) {
    user = User.findOne({
      email: req.body.email
    })
  } else if (req.body.policy) {
    user = User.findOne({
      policy: req.body.policy
    })
  }
  if (!user) {
    res.status(404).send({
      message: 'Пользователь не найден',
      hasError: true,
      payload: null
    });
    return;
  }
  user
    .populate("roles", "-__v")
    .exec((err, user) => {
      if (err) {
        res.status(500).send({
          message: err.message,
          hasError: true,
          payload: null
        });
        return;
      }

      if (!user) {
        return res.status(404).send({
          message: "Пользователь не найден",
          hasError: true,
          payload: null
        });
      }

      let passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password
      );

      if (!passwordIsValid) {
        return res.status(401).send({
          message: "Неверный пароль",
          hasError: true,
          payload: null
          
        });
      }

      let token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: 86400 // 24 hours
      });

      let authorities = [];

      for (let i = 0; i < user.roles.length; i++) {
        authorities.push("ROLE_" + user.roles[i].name.toUpperCase());
      }
      res.status(200).send({
        message: '',
        hasError: false,
        payload: {
          email: user.email,
          name: user.name,
          surname: user.surname,
          patronymic: user.patronymic,
          policy: user.policy,
          age: user.age,
          imt: user.imt,
          alcoholIndex: user.alcoholIndex,
          smokingIndex: user.smokingIndex,
          roles: authorities,
          accessToken: token,
          gender: user.gender,
        }
      });
    });
};
