const mongoose = require("mongoose");

const Doctor = mongoose.model(
  "Doctor",
  new mongoose.Schema({
    name:  {
      type: String,
      required: true
    },
    surname: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    startWork: {
      type: String,
    },
    password: {
      type: String,
      reuqired: true,
    },
    specialities: [
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: "Speciality",
          required: true
        }
    ],
    canOnline: Boolean
  }, { timestamps: true })
);

module.exports = Doctor;


/**
 * @swagger
 * components:
 *   schemas:
 *     Doctor:
 *       type: object
 *       required:
 *        - name
 *        - surname
 *        - lastName
 *        - email
 *        - clinics
 *        - specialities
 *       properties:
 *         name:
 *          type: string
 *          description: Имя пользователя.
 *          example: Владимир
 *         surname:
 *          type: string
 *          description: Фамилия пользователя.
 *          example: Власов
 *         lastName:
 *          type: string
 *          description: Отчество пользователя.
 *          example: Владимирович
 *         email:
 *          type: string
 *          description: Почта.
 *          example: user@mail.ru
 *         startWork:
 *          type: string
 *          description: Время начала работы (Отсчет стажа)
 *          example: ISO Time
 *         specialities:
 *          type: array
 *          items:
 *            $ref: '#/components/schemas/Speciality'
 *          
 */
