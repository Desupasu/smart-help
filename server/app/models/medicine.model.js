const mongoose = require("mongoose");

const Medicine = mongoose.model(
  "Medicine",
  new mongoose.Schema({
    name: {
      type: String,
      require: true,
    },
    description: String
  }, { timestamps: true })
);

module.exports = Medicine;

/**
 * @swagger
 * components:
 *   schemas:
 *     Medicine:
 *       type: object
 *       required:
 *        - name
 *       properties:
 *         name:
 *          type: string
 *          description: Наименование препарата
 *          example: Арбидол
 *         description:
 *          type: string
 *          description: Описание препарата
 */