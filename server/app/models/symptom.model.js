const mongoose = require("mongoose");

const Symptom = mongoose.model(
  "Symptom",
  new mongoose.Schema({
    name: {
      type: String,
      required: true
    },
    description: String
  }, { timestamps: true })
);

module.exports = Symptom;

/**
 * @swagger
 * components:
 *   schemas:
 *     Symptom:
 *       type: object
 *       required:
 *        - name
 *       properties:
 *         name:
 *          type: string
 *          description: Наименование симптома
 *          example: головная боль
 *         description:
 *          type: string
 *          description: Описание симптома
 */