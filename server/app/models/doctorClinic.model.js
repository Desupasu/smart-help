const mongoose = require("mongoose");

const DoctorClinic = mongoose.model(
  "DoctorClinic",
  new mongoose.Schema({
    doctor: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Doctor",
        required: true
    },
    clinic: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Clinic",
        required: true
    },
    canOnline: {
        type: Boolean,
        required: false
    }
  }, { timestamps: true })
);

module.exports = DoctorClinic;

/**
 * @swagger
 * components:
 *   schemas:
 *     DoctorClinic:
 *       type: object
 *       required:
 *         - canOnline
 *         - doctor
 *         - clinic
 *       properties:
 *         canOnline:
 *           type: boolean
 *           description: Может ли работать онлайн
 *         doctor:
 *           $ref: '#/components/schemas/Doctor'
 *         clinic:
 *           $ref: '#/components/schemas/Clinic'
 */
