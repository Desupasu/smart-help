const mongoose = require("mongoose");

const Desease = mongoose.model(
  "Desease",
  new mongoose.Schema({
    name:  {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: true,
    },
    mainDoctor: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Speciality",
        required: true
    },
    reasons: [
        {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Reason",
        required: true
        }
    ],
    symptoms: [
        {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Symptom",
        required: true
        }
    ],
    doctors: [
        {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Speciality",
        required: true
        }
    ],
  }, { timestamps: true })
);

module.exports = Desease;

/**
 * @swagger
 * components:
 *   schemas:
 *     Desease:
 *       type: object
 *       properties:
 *         name:
 *          type: string
 *          description: Название болезни.
 *          example: Сальмонелез
 *         description:
 *          type: string
 *          description: Описание болезни.
 *          example: Сальмонелез - это ...
 *         mainDoctor:
 *          $ref: '#/components/schemas/Speciality'
 *         reasons:
 *          type: array
 *          items:
 *           $ref: '#/components/schemas/Reason'
 *         symptoms:
 *          type: array
 *          items:
 *           $ref: '#/components/schemas/Symptom'
 *         doctors:
 *          type: array
 *          items:
 *           $ref: '#/components/schemas/Speciality'
 *          
 */