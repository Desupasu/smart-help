const mongoose = require("mongoose");

const Role = mongoose.model(
  "Role",
  new mongoose.Schema({
    name: {
      type: String,
      required: true
    },
  }, { timestamps: true })
);

module.exports = Role;


/**
 * @swagger
 * components:
 *   schemas:
 *     Role:
 *       type: object
 *       required:
 *        - name
 *       properties:
 *         name:
 *          type: string
 *          description: Название роли
 *          example: ROLE_USER
 */