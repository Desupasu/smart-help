const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const db = {};

db.mongoose = mongoose;

db.user = require("./user.model");
db.role = require("./role.model");
db.speciality = require("./speciality.model");
db.reason = require("./reason.model");
db.symptom = require("./symptom.model");
db.desease = require("./desease.model");
db.medicine = require("./medicine.model");
db.clinic = require("./clinic.model");
db.doctor = require("./doctor.model");
db.doctorClinic = require("./doctorClinic.model");
db.slot = require("./slot.model");
db.meet = require("./meet.model");

db.ROLES = ["user", "doctor", "nurse",  "admin", "moderator"];

module.exports = db;