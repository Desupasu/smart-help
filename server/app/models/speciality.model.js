const mongoose = require("mongoose");

const Speciality = mongoose.model(
  "Speciality",
  new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: String
  }, { timestamps: true })
);

module.exports = Speciality;

/**
 * @swagger
 * components:
 *   schemas:
 *     Speciality:
 *       type: object
 *       required:
 *        - name
 *       properties:
 *         name:
 *          type: string
 *          description: Наименование специальности
 *          example: ЛОР
 *         description:
 *          type: string
 *          description: Описание специальности
 */