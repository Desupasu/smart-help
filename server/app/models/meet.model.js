const mongoose = require("mongoose");

const Meet = mongoose.model(
  "Meet",
  new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    slot: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Slot",
        required: true
    },
    speciality: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Speciality",
        required: true
    },
    symptoms: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Symptom",
        required: false
    },]
  }, { timestamps: true })
);

module.exports = Meet;


/**
 * @swagger
 * components:
 *   schemas:
 *     Meet:
 *       type: object
 *       required:
 *          - user
 *          - speciality
 *          - slot
 *       properties:
 *         user:
 *           $ref: '#/components/schemas/User'
 *         speciality:
 *           $ref: '#/components/schemas/Speciality'
 *         slot:
 *           $ref: '#/components/schemas/Slot'
 *         symptoms:
 *           type: array
 *           items:
 *             $ref: '#/components/schemas/Symptom'
 *          
 */
