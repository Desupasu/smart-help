const mongoose = require("mongoose");

const Slot = mongoose.model(
  "Slot",
  new mongoose.Schema({
    doctorClinic: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "DoctorClinic",
        required: true
    },
    isOnline: {
        type: Boolean,
        required: true,
    },
    isActive: {
        type: Boolean,
        required: true,
    },
    endDate: {
        type: String,
        required: true,
    },
    stDate:  {
        type: String,
        required: true,
    },
  }, { timestamps: true })
);

module.exports = Slot;


/**
 * @swagger
 * components:
 *   schemas:
 *     Slot:
 *       type: object
 *       required:
 *         - doctorClinic
 *         - isOnline
 *         - isActive
 *         - endDate
 *         - stDate
 *       properties:
 *         isActive:
 *          type: boolean
 *          description: Свободна ли запись.
 *         isOnline:
 *          type: boolean
 *          description: Запись онлайн
 *         endDate:
 *          type: string
 *          description: ДатаВремя начала приема
 *          example: 2022-04-14T14:15:00
 *         stDate:
 *          type: string
 *          description: ДатаВремя конца приема
 *          example: 2022-04-14T14:15:00
 *          
 */
