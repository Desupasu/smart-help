const mongoose = require("mongoose");

const User = mongoose.model(
  "User",
  new mongoose.Schema({
    name:  {
      type: String,
      required: true
    },
    surname: {
      type: String,
      required: true,
    },
    patronymic: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    policy: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      reuqired: true,
    },
    roles: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Role",
        required: true
      }
    ],
    gender: Boolean,
    age: Number,
    imt: Number,
    alcoholIndex: Number,
    smokingIndex: Number,
    chronicDeseases: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Desease",
        required: true
      }
    ],
    usedMedicine: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Medicine",
        required: true
      }
    ]
  }, { timestamps: true })
);

module.exports = User;

/**
 * @swagger
 * components:
 *   schemas:
 *     RequiredUser:
 *       type: object
 *       required:
 *        - name
 *        - surname
 *        - patronymic
 *        - email
 *        - policy
 *        - roles
 *       properties:
 *         name:
 *          type: string
 *          description: Имя пользователя.
 *          example: Владимир
 *         surname:
 *          type: string
 *          description: Фамилия пользователя.
 *          example: Власов
 *         patronymic:
 *          type: string
 *          description: Отчество пользователя.
 *          example: Владимирович
 *         email:
 *          type: string
 *          description: Почта.
 *          example: user@mail.ru
 *         policy:
 *          type: string
 *          description: Полис медицинский.
 *          example: 234523452345
 *         roles:
 *          type: array
 *          items:
 *           type: string
 *           example: ROLE_USER
 *          
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     NonRequiredUser:
 *       type: object
 *       properties:
 *         gender:
 *          type: boolean
 *          description: Пол true - мужчина.
 *          example: true
 *         age:
 *          type: number
 *          description: Возраст.
 *          example: 25
 *         imt:
 *          type: number
 *          description: ИМТ
 *          example: 18
 *         alcoholIndex:
 *          type: number
 *          description: Отношение к алкоголю 0 - не указано 1 - резко негативное ... 5 - положительное. и т.д.
 *          example: 3
 *         smokingIndex:
 *          type: number
 *          description: Отношение к алкоголю 0 - не указано, 1 - резко негативное, ... 5 - положительное. и т.д.
 *          example: 3
 *         chronicDeseases:
 *          type: array
 *          items:
 *           $ref: '#/components/schemas/Desease'
 *         usedMedicine:
 *          type: array
 *          items:
 *           $ref: '#/components/schemas/Medicine'
 *          
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     User:
 *       allOf:
 *        - $ref: '#/components/schemas/RequiredUser'
 *        - $ref: '#/components/schemas/NonRequiredUser'
 */