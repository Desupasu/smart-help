const mongoose = require("mongoose");

const Clinic = mongoose.model(
  "Clinic",
  new mongoose.Schema({
    name: {
      type: String,
      required: true
    },
    address: {
        type: String,
        required: true
    },
    workTime: [
        {
            name: {
                type: String,
                required: true
            },
            st_date: {
                type: String,
                required: true
            },
            end_date: {
                type: String,
                required: true
            },
        }
    ]
  }, { timestamps: true })
);

module.exports = Clinic;

/**
 * @swagger
 * components:
 *   schemas:
 *     Clinic:
 *       type: object
 *       properties:
 *         name:
 *          type: string
 *          description: Название клиники.
 *          example: Детский медицинский центр Ваш доктор
 *         address:
 *          type: string
 *          description: Адрес клиники.
 *          example: г. Нижний Новгород, Ильича, д. 25
 *         workTime:
 *          type: array
 *          items:
 *            type: object
 *            properties:
 *              name:
 *                type: string
 *                description: День недели
 *              st_date:
 *                type: string
 *                description: Время начала работы
 *              end_date:
 *                type: string
 *                description: Время конца работы          
 *          
 */