const mongoose = require("mongoose");

const Reason = mongoose.model(
  "Reason",
  new mongoose.Schema({
    name: {
      type: String,
      require: true,
    },
    description: String
  }, { timestamps: true })
);

module.exports = Reason;

/**
 * @swagger
 * components:
 *   schemas:
 *     Reason:
 *       type: object
 *       required:
 *        - name
 *       properties:
 *         name:
 *          type: string
 *          description: Наименование причины болезни
 *          example: избыточный вес
 *         description:
 *          type: string
 *          description: Описание причины болезни
 */